/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       UserM.c
 *    \author     Alexandra Purcaru
 *    \brief      Implements the functions for adding a new user, modifying an existing user and deleting an
 *                existing user.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "UserM.h"
#include "UserM_Cfg.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#define USERM_NR_OF_INSTANCES       (4U)
#define USERM_SIZE_OF_BUFFER        (12U)

#define USERM_LENGTH_USER           (10U)
//#define USERM_IN_LIST_PARAMETERS_OFFSET   ()

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

typedef enum
{
   USERM_READ,
   USERM_WRITE,
} UserM_StateType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

static Rte_ParameterType UserM_auc_WriteBuffer1;
static Rte_ParameterType UserM_auc_WriteBuffer2;
static Rte_ParameterType UserM_auc_WriteBuffer3;
static UserM_StateType UserM_t_State;

static uint8 UserM_uc_ReadIndex;
static uint8 UserM_uc_WriteIndex;
static uint8 UserM_uc_FeeIndex;
static uint8 UserM_auc_UserBuffer1[USERM_LENGTH_USER];
static uint8 UserM_auc_UserBuffer2[USERM_LENGTH_USER];
static uint8 UserM_auc_UserBuffer3[USERM_LENGTH_USER];

static uint8 UserM_auc_MemoryBuffer1[USERM_LENGTH_USER];
static uint8 UserM_auc_MemoryBuffer2[USERM_LENGTH_USER];
static uint8 UserM_auc_MemoryBuffer3[USERM_LENGTH_USER];

static boolean UserM_b_WritePermission = FALSE;

//static boolean UserM_b_WriteState = FALSE;
//static uint8 UserM_uc_WriteUser;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      The functions reads the values written in the flash memory.
 * \param      .
 * \return     -
 */
void UserM_gv_Init(void)
{
   uint8 uc_I;
   for ((uc_I = 0); (uc_I < USERM_LENGTH_USER); (uc_I++))
   {
      UserM_auc_UserBuffer1[uc_I] = 0x00;
      UserM_auc_UserBuffer2[uc_I] = 0x00;
      UserM_auc_UserBuffer3[uc_I] = 0x00;
   }

   UserM_uc_ReadIndex = 0U;
   UserM_uc_WriteIndex = 0U;
   UserM_uc_FeeIndex = 0U;
   UserM_t_State = USERM_READ;
}

/**
 * \brief      The function writes the data in the flash when it is ready.
 * \param      -
 * \return     -
 */
void UserM_gv_Main(void)
{
   static boolean b_FlashState = E_NOT_OK;
   uint8 uc_I = 0U;
//   static uint8 uc_WriteCounter = 0;
   Rte_ParameterType t_UserParameter1;
   Rte_ParameterType t_UserParameter2;
   Rte_ParameterType t_UserParameter3;

   switch (UserM_t_State)
   {
      case (USERM_READ):
      {
         /* READ OPERATION. */

         if (UserM_uc_ReadIndex == 0U)
         {
            Fls_gv_Read(UserM_kat_StartAddresses[UserM_uc_ReadIndex], (uint8*) &UserM_auc_UserBuffer1,
            USERM_SIZE_OF_BUFFER);
            t_UserParameter1.t_Tag = ((UserM_auc_UserBuffer1[uc_I] << 24U) | (UserM_auc_UserBuffer1[uc_I + 1U] << 16U)
               | (UserM_auc_UserBuffer1[uc_I + 2U] << 8U) | (UserM_auc_UserBuffer1[uc_I + 3U]));
            t_UserParameter1.t_DutyLeft =
               ((UserM_auc_UserBuffer1[uc_I + 4U] << 8U) | (UserM_auc_UserBuffer1[uc_I + 5U]));
            t_UserParameter1.t_DutyRight = ((UserM_auc_UserBuffer1[uc_I + 6U] << 8U)
               | (UserM_auc_UserBuffer1[uc_I + 7U]));
            t_UserParameter1.t_DutyMiddle = ((UserM_auc_UserBuffer1[uc_I + 8U] << 8U)
               | (UserM_auc_UserBuffer1[uc_I + 9U]));
            UserM_gm_ReadParameters(&t_UserParameter1)
            ;

            UserM_uc_ReadIndex++;

         }
         else if (UserM_uc_ReadIndex == 1U)
         {
            Fls_gv_Read(UserM_kat_StartAddresses[UserM_uc_ReadIndex], (uint8*) &UserM_auc_UserBuffer2,
            USERM_SIZE_OF_BUFFER);

            t_UserParameter2.t_Tag = ((UserM_auc_UserBuffer2[uc_I] << 24U) | (UserM_auc_UserBuffer2[uc_I + 1U] << 16U)
               | (UserM_auc_UserBuffer2[uc_I + 2U] << 8U) | (UserM_auc_UserBuffer2[uc_I + 3U]));
            t_UserParameter2.t_DutyLeft =
               ((UserM_auc_UserBuffer2[uc_I + 4U] << 8U) | (UserM_auc_UserBuffer2[uc_I + 5U]));
            t_UserParameter2.t_DutyRight = ((UserM_auc_UserBuffer2[uc_I + 6U] << 8U)
               | (UserM_auc_UserBuffer2[uc_I + 7U]));
            t_UserParameter2.t_DutyMiddle = ((UserM_auc_UserBuffer2[uc_I + 8U] << 8U)
               | (UserM_auc_UserBuffer2[uc_I + 9U]));
            UserM_gm_ReadParameters(&t_UserParameter2)
            ;

            UserM_uc_ReadIndex++;

         }
         else
         {
            Fls_gv_Read(UserM_kat_StartAddresses[UserM_uc_ReadIndex], (uint8*) &UserM_auc_UserBuffer3,
            USERM_SIZE_OF_BUFFER);

            t_UserParameter3.t_Tag = ((UserM_auc_UserBuffer3[uc_I] << 24U) | (UserM_auc_UserBuffer3[uc_I + 1U] << 16U)
               | (UserM_auc_UserBuffer3[uc_I + 2U] << 8U) | (UserM_auc_UserBuffer3[uc_I + 3U]));
            t_UserParameter3.t_DutyLeft =
               ((UserM_auc_UserBuffer3[uc_I + 4U] << 8U) | (UserM_auc_UserBuffer3[uc_I + 5U]));
            t_UserParameter3.t_DutyRight = ((UserM_auc_UserBuffer3[uc_I + 6U] << 8U)
               | (UserM_auc_UserBuffer3[uc_I + 7U]));
            t_UserParameter3.t_DutyMiddle = ((UserM_auc_UserBuffer3[uc_I + 8U] << 8U)
               | (UserM_auc_UserBuffer3[uc_I + 9U]));
            UserM_gm_ReadParameters(&t_UserParameter3)
            ;

            UserM_uc_ReadIndex = 0U;

            UserM_t_State = USERM_WRITE;

         }
         break;
      }
      case (USERM_WRITE):
      {
         /* WRITE OPERATION. */

         if (Rte_MemoryWrite_MemoryOut_Write_UpdateFlag == TRUE)
         {
            if (UserM_uc_WriteIndex == 0U)
            {
               Rte_Read_MemoryOut_Write(&UserM_auc_WriteBuffer1);

               UserM_auc_MemoryBuffer1[uc_I] = UserM_auc_WriteBuffer1.t_Tag >> 24U;
               UserM_auc_MemoryBuffer1[uc_I + 1U] = UserM_auc_WriteBuffer1.t_Tag >> 16U;
               UserM_auc_MemoryBuffer1[uc_I + 2U] = UserM_auc_WriteBuffer1.t_Tag >> 8U;
               UserM_auc_MemoryBuffer1[uc_I + 3U] = UserM_auc_WriteBuffer1.t_Tag;
               UserM_auc_MemoryBuffer1[uc_I + 4U] = UserM_auc_WriteBuffer1.t_DutyLeft >> 8U;
               UserM_auc_MemoryBuffer1[uc_I + 5U] = UserM_auc_WriteBuffer1.t_DutyLeft;
               UserM_auc_MemoryBuffer1[uc_I + 6U] = UserM_auc_WriteBuffer1.t_DutyRight >> 8U;
               UserM_auc_MemoryBuffer1[uc_I + 7U] = UserM_auc_WriteBuffer1.t_DutyRight;
               UserM_auc_MemoryBuffer1[uc_I + 8U] = UserM_auc_WriteBuffer1.t_DutyMiddle >> 8U;
               UserM_auc_MemoryBuffer1[uc_I + 9U] = UserM_auc_WriteBuffer1.t_DutyMiddle;

               UserM_uc_WriteIndex++;
            }
            else if (UserM_uc_WriteIndex == 1U)
            {
               Rte_Read_MemoryOut_Write(&UserM_auc_WriteBuffer2);

               UserM_auc_MemoryBuffer2[uc_I] = UserM_auc_WriteBuffer2.t_Tag >> 24U;
               UserM_auc_MemoryBuffer2[uc_I + 1U] = UserM_auc_WriteBuffer2.t_Tag >> 16U;
               UserM_auc_MemoryBuffer2[uc_I + 2U] = UserM_auc_WriteBuffer2.t_Tag >> 8U;
               UserM_auc_MemoryBuffer2[uc_I + 3U] = UserM_auc_WriteBuffer2.t_Tag;
               UserM_auc_MemoryBuffer2[uc_I + 4U] = UserM_auc_WriteBuffer2.t_DutyLeft >> 8U;
               UserM_auc_MemoryBuffer2[uc_I + 5U] = UserM_auc_WriteBuffer2.t_DutyLeft;
               UserM_auc_MemoryBuffer2[uc_I + 6U] = UserM_auc_WriteBuffer2.t_DutyRight >> 8U;
               UserM_auc_MemoryBuffer2[uc_I + 7U] = UserM_auc_WriteBuffer2.t_DutyRight;
               UserM_auc_MemoryBuffer2[uc_I + 8U] = UserM_auc_WriteBuffer2.t_DutyMiddle >> 8U;
               UserM_auc_MemoryBuffer2[uc_I + 9U] = UserM_auc_WriteBuffer2.t_DutyMiddle;

               UserM_uc_WriteIndex++;
            }
            else if (UserM_uc_WriteIndex == 2U)
            {
               Rte_Read_MemoryOut_Write(&UserM_auc_WriteBuffer3);

               UserM_auc_MemoryBuffer3[uc_I] = UserM_auc_WriteBuffer3.t_Tag >> 24U;
               UserM_auc_MemoryBuffer3[uc_I + 1U] = UserM_auc_WriteBuffer3.t_Tag >> 16U;
               UserM_auc_MemoryBuffer3[uc_I + 2U] = UserM_auc_WriteBuffer3.t_Tag >> 8U;
               UserM_auc_MemoryBuffer3[uc_I + 3U] = UserM_auc_WriteBuffer3.t_Tag;
               UserM_auc_MemoryBuffer3[uc_I + 4U] = UserM_auc_WriteBuffer3.t_DutyLeft >> 8U;
               UserM_auc_MemoryBuffer3[uc_I + 5U] = UserM_auc_WriteBuffer3.t_DutyLeft;
               UserM_auc_MemoryBuffer3[uc_I + 6U] = UserM_auc_WriteBuffer3.t_DutyRight >> 8U;
               UserM_auc_MemoryBuffer3[uc_I + 7U] = UserM_auc_WriteBuffer3.t_DutyRight;
               UserM_auc_MemoryBuffer3[uc_I + 8U] = UserM_auc_WriteBuffer3.t_DutyMiddle >> 8U;
               UserM_auc_MemoryBuffer3[uc_I + 9U] = UserM_auc_WriteBuffer3.t_DutyMiddle;

               UserM_b_WritePermission = TRUE;
               UserM_uc_WriteIndex++;
            }
            else
            {
            }
         }
         else
         {

         }

         if (UserM_b_WritePermission == TRUE)
         {
            if (UserM_uc_FeeIndex == 0U)
            {
               if (b_FlashState == E_NOT_OK)
               {
                  b_FlashState = Fee_gv_Write(UserM_uc_FeeIndex, (uint8*) &UserM_auc_MemoryBuffer1);
               }
               else
               {
                  UserM_uc_FeeIndex++;
                  b_FlashState = E_NOT_OK;
               }
            }
            else if (UserM_uc_FeeIndex == 1U)
            {
               if (b_FlashState == E_NOT_OK)
               {
                  b_FlashState = Fee_gv_Write(UserM_uc_FeeIndex, (uint8*) &UserM_auc_MemoryBuffer2);
               }
               else
               {
                  UserM_uc_FeeIndex++;
                  b_FlashState = E_NOT_OK;
               }
            }
            else if (UserM_uc_FeeIndex == 2U)
            {
               if (b_FlashState == E_NOT_OK)
               {
                  b_FlashState = Fee_gv_Write(UserM_uc_FeeIndex, (uint8*) &UserM_auc_MemoryBuffer3);
               }
               else
               {
                  UserM_uc_FeeIndex++;
               }
            }
            else
            {
                  UserM_b_WritePermission = FALSE;
            }
         }
         else
         {
            /* Nothing happens. */
         }

//         {
//            UserM_uc_WriteUser = 0U;
//
//            if (UserM_uc_WriteIndex == 0U)
//            {
//               Rte_Read_MemoryOut_Write(&UserM_gat_UserParam);
//
//               UserM_auc_WriteBuffer[uc_I] = UserM_gat_UserParam.t_Tag >> 24U;
//               UserM_auc_WriteBuffer[uc_I + 1U] = UserM_gat_UserParam.t_Tag >> 16U;
//               UserM_auc_WriteBuffer[uc_I + 2U] = UserM_gat_UserParam.t_Tag >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 3U] = UserM_gat_UserParam.t_Tag;
//               UserM_auc_WriteBuffer[uc_I + 4U] = UserM_gat_UserParam.t_DutyLeft >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 5U] = UserM_gat_UserParam.t_DutyLeft;
//               UserM_auc_WriteBuffer[uc_I + 6U] = UserM_gat_UserParam.t_DutyRight >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 7U] = UserM_gat_UserParam.t_DutyRight;
//               UserM_auc_WriteBuffer[uc_I + 8U] = UserM_gat_UserParam.t_DutyMiddle >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 9U] = UserM_gat_UserParam.t_DutyMiddle;
//
//               if (b_FlashState == E_NOT_OK)
//               {
//                  b_FlashState = Fee_gv_Write(UserM_uc_WriteUser, (uint8*) &UserM_auc_WriteBuffer);
//               }
//               else
//               {
//                  UserM_uc_WriteIndex++;
//               }
//            }
//            else if ((UserM_uc_WriteIndex == 1U) && (b_FlashState == E_OK))
//            {
//               b_FlashState = E_NOT_OK;
//               Rte_Read_MemoryOut_Write(&UserM_gat_UserParam);
//
//               UserM_auc_WriteBuffer[uc_I + 10U] = UserM_gat_UserParam.t_Tag >> 24U;
//               UserM_auc_WriteBuffer[uc_I + 11U] = UserM_gat_UserParam.t_Tag >> 16U;
//               UserM_auc_WriteBuffer[uc_I + 12U] = UserM_gat_UserParam.t_Tag >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 13U] = UserM_gat_UserParam.t_Tag;
//               UserM_auc_WriteBuffer[uc_I + 14U] = UserM_gat_UserParam.t_DutyLeft >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 15U] = UserM_gat_UserParam.t_DutyLeft;
//               UserM_auc_WriteBuffer[uc_I + 16U] = UserM_gat_UserParam.t_DutyRight >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 17U] = UserM_gat_UserParam.t_DutyRight;
//               UserM_auc_WriteBuffer[uc_I + 18U] = UserM_gat_UserParam.t_DutyMiddle >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 19U] = UserM_gat_UserParam.t_DutyMiddle;
//
//               UserM_uc_WriteIndex++;
//            }
//            else if (UserM_uc_WriteIndex == 2U)
//            {
//               Rte_Read_MemoryOut_Write(&UserM_gat_UserParam);
//
//               UserM_auc_WriteBuffer[uc_I + 20U] = UserM_gat_UserParam.t_Tag >> 24U;
//               UserM_auc_WriteBuffer[uc_I + 21U] = UserM_gat_UserParam.t_Tag >> 16U;
//               UserM_auc_WriteBuffer[uc_I + 22U] = UserM_gat_UserParam.t_Tag >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 23U] = UserM_gat_UserParam.t_Tag;
//               UserM_auc_WriteBuffer[uc_I + 24U] = UserM_gat_UserParam.t_DutyLeft >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 25U] = UserM_gat_UserParam.t_DutyLeft;
//               UserM_auc_WriteBuffer[uc_I + 26U] = UserM_gat_UserParam.t_DutyRight >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 27U] = UserM_gat_UserParam.t_DutyRight;
//               UserM_auc_WriteBuffer[uc_I + 28U] = UserM_gat_UserParam.t_DutyMiddle >> 8U;
//               UserM_auc_WriteBuffer[uc_I + 29U] = UserM_gat_UserParam.t_DutyMiddle;
//
//               UserM_uc_WriteIndex++;
//            }
//            else
//            {
//               UserM_b_WriteState = TRUE;
//            }
//
//            if (UserM_b_WriteState == TRUE)
//            {
//               if (b_FlashState == E_NOT_OK)
//               {
//                  b_FlashState = Fee_gv_Write(UserM_uc_WriteUser, (uint8*) &UserM_auc_WriteBuffer);
//                  uc_WriteCounter++;
//               }
//               else
//               {
//                  UserM_b_WriteState = FALSE;
//               }
//            }
////            if ((UserM_b_WriteState == TRUE) && (uc_WriteCounter < 10))
////            {
////               if (b_FlashState == E_NOT_OK)
////               {
////                  b_FlashState = Fee_gv_Write(UserM_uc_WriteUser, (uint8*) &UserM_auc_WriteBuffer);
////                  uc_WriteCounter++;
////               }
////               else
////               {
////                  UserM_uc_WriteUser++;
////                  uc_WriteCounter = 0U;
////               }
////            }
////            else
////            {
////            }
//
////            if ((uc_WriteCounter == 10U) && (UserM_uc_WriteUser == 2U))
////            {
////
////            }
////            else
////            {
////               UserM_b_WriteState = FALSE;
////            }
//
//            break;
//         }
      }
   }
}
