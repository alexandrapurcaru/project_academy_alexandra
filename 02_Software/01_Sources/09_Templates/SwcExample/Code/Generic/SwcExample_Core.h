/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       SwcExample_Core.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Exports all the global types, macros, variables, constants and functions that are visible only inside
 *                the software components.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef SWCEXAMPLE_CORE_H
#define SWCEXAMPLE_CORE_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Rte_SwcExample.h"
#include "SwcExample_Cfg.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern IoHwAb_DigitalLevelType SwcExample_gat_InputValues[SWCEXAMPLE_NUMBER_OF_INSTANCES];
extern Rte_BtnState SwcExample_gat_OutputValues[SWCEXAMPLE_NUMBER_OF_INSTANCES];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const IoHwAb_DigitalLevelType SwcExample_gkat_ActiveValues[SWCEXAMPLE_NUMBER_OF_INSTANCES];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void SwcExample_gv_RteInputUpdate(void);
extern void SwcExample_gv_RteOutputUpdate(void);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* SWCEXAMPLE_CORE_H */
