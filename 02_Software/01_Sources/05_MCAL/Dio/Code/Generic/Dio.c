/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Dio.c
 *    \author     Alexandra Purcaru
 *    \brief      Implements the DIO interfaces.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Dio.h"
#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      The function reads the value of a specified DIO channel.
 * \param      ChannelId: ID of DIO channel
 * \return     The physical level of the corresponding Pin.
 */
Dio_LevelType Dio_ReadChannel(Dio_ChannelType ChannelId)
{
   Dio_LevelType t_Return;
   uint16 us_Level;

   us_Level = ((Dio_gkat_ChannelConfig[ChannelId].pt_BaseAdress->IDR & Dio_gkat_ChannelConfig[ChannelId].us_Mask));

   if ((us_Level & Dio_gkat_ChannelConfig[ChannelId].us_Mask) != 0U)

   {
      t_Return = STD_HIGH;
   }
   else
   {
      t_Return = STD_LOW;
   }
   return t_Return;
}

/**
 * \brief      The function sets the specified level for the specified channel.
 * \param      ChannelId: ID of DIO channel.
 * \param      Level: Value to be written.
 * \return     -
 */
void Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level)
{
   if (Level == STD_HIGH)
   {
      Dio_gkat_ChannelConfig[ChannelId].pt_BaseAdress->ODR |= Dio_gkat_ChannelConfig[ChannelId].us_Mask;
   }
   else
   {
      Dio_gkat_ChannelConfig[ChannelId].pt_BaseAdress->ODR &= ~(Dio_gkat_ChannelConfig[ChannelId].us_Mask);
   }
}

/**
 * \brief      The function reads the value of all channels of a specified DIO port.
 * \param      PortId: ID of DIO port
 * \return     The level of all channels of that port.
 */
Dio_PortLevelType Dio_ReadPort(Dio_PortType PortId)
{
   Dio_PortLevelType t_Level;

   t_Level = (Dio_PortLevelType) Dio_gkat_ChannelConfig[PortId].pt_BaseAdress->IDR;

   return t_Level;
}

/**
 * \brief      The function sets the specified value for the specified port.
 * \param      PortId: ID of DIO Port.
 * \param      Level: Value to be written.
 * \return     -
 */
void Dio_WritePort(Dio_PortType PortId, Dio_PortLevelType Level)
{
   Dio_gkat_ChannelConfig[PortId].pt_BaseAdress->ODR = (uint32) Level;
}
