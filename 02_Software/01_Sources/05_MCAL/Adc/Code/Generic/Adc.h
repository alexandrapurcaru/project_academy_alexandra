/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Adc.h
 *    \author     Alexandra Purcaru
 *    \brief      Exports the data types and the interfaces used for implementations.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef ADC_H
#define ADC_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "Adc_Cfg.h"
#include "stm32f407xx.h"
#include "RegInit.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defining the channel configuration table, creating the link between the base address and the value to be
 *          written in the register. */
typedef struct
{
   /** \brief  Defines the base address of the hardware ADC peripheral. */
   ADC_TypeDef* pt_BaseAddress;
   /** \brief  Defines the channel number and the sequence to be assigned in the conversion. */
   uint32 ul_ChannelNumber;
} Adc_ChannelConfigType;

/** \brief  Defining the configuration table.*/
typedef struct
{
   /** \brief  Defines the pointer to the initialization registers. */
   const RegInit_Masked32BitsConfigType* kpt_Registers;
   /** \brief  Defines the pointer to the channel configuration registers. */
   const Adc_ChannelConfigType* kpt_Channels;
} Adc_ConfigType;

/** \brief  Defines the numeric ID of an ADC channel group. */
typedef uint8 Adc_GroupType;

/** \brief  Defines the maximum width of a conversion. */
typedef uint16 Adc_ValueGroupType;

/** \brief  Defines the current status of the conversion of the requested ADC Channel group. */
typedef enum
{
   /** \brief  The conversion of the specified group has not been started- no result is available. */
   ADC_IDLE,
   /** \brief  The conversion of the specified group has been started and is still going on- no result is available. */
   ADC_BUSY,
   /** \brief  A conversion of the specified group has been finished- a result is available for all channels. */
   ADC_COMPLETED,
   /** \brief  The result buffer is completely filled- for each selected channel the number of samples is available. */
   ADC_STREAM_COMPLETED,
} Adc_StatusType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const Adc_ConfigType Adc_gkt_Config;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void Adc_Init(const Adc_ConfigType* ConfigPtr);
extern Std_ReturnType Adc_SetupResultBuffer(Adc_GroupType Group, const Adc_ValueGroupType* DataBufferPtr);
extern void Adc_StartGroupConversion(Adc_GroupType Group);
extern Adc_StatusType Adc_GetGroupStatus(Adc_GroupType Group);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* ADC_H */
