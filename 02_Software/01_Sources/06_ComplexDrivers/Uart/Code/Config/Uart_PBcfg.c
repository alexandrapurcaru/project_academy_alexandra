/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Uart_PBcfg.c
 *    \author     Alexandra Purcaru
 *    \brief      Defines the control registers list for UART peripherals configuration.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Uart.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the total number of configuration registers. */
#define UART_LOCAL_NR_OF_REGISTERS     (7U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the configuration registers array. */
static const RegInit_Masked32BitsSingleType Uart_kat_ChannelsConfig[UART_LOCAL_NR_OF_REGISTERS] =
   {
      /*    Enables USART. */
      { & USART3->CR1, ~(USART_CR1_UE), USART_CR1_UE },
      /*    Sets the word length at 8 bits. */
      { & USART3->CR1, ~(USART_CR1_M), 0x00 },
      /*    Sets 1 stop bit. */
      { & USART3->CR2, ~(USART_CR2_STOP), 0X00 },
      /*    Sets oversampling by 16: low speed and high tolerance to the transfer. */
      { & USART3->CR1, ~(USART_CR1_OVER8), 0x00 },
      /*    BaudRate = (fCLK/(8*(2-OVER8)*USARTDIV). For the desired baud rate, USARTDIV will get a calculated value.
       *    The real number will be Mantissa and the fractional number multiplied with the selected oversampling
       *    will be the Fraction. */
      /*    Sets the baud rate at 256Kbps. */
//      { & USART3->BRR, ~(USART_BRR_DIV_Mantissa | USART_BRR_DIV_Fraction), 0x9C },
      /*    Sets the baud rate at 115200 bps. */
      { & USART3->BRR, ~(USART_BRR_DIV_Mantissa | USART_BRR_DIV_Fraction), 0x15B },
      /*    Enable USART Tx. */
      { & USART3->CR1, ~(USART_CR1_TE), USART_CR1_TE },
      /*    Enable USART Rx. */
      { & USART3->CR1, ~(USART_CR1_RE), USART_CR1_RE },
   };

/** \brief  Defines the base address configuration table.. */
static const Uart_ChannelConfigType Uart_kat_Channels[UART_NR_OF_CHANNELS] =
   {
      /*    Sets the configuration USART channels. */
      { USART3 },
      { USART3 },
   };

/** \brief  Defines the array containing the configuration registers and the number of registers. */
static const RegInit_Masked32BitsConfigType Uart_kt_Config =
   { Uart_kat_ChannelsConfig, UART_LOCAL_NR_OF_REGISTERS };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the structure containing the configuration registers and base address for every configured channel. */
const Uart_ConfigType Uart_gkt_Config =
   { &Uart_kt_Config, Uart_kat_Channels };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

