/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte.c
 *    \author     RTE Generator
 *    \brief      Generated file - shall not be manually edited. 
 *                Implements the RTE initialization function which loads the RTE buffers with start values.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Rte.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
/**
 * \brief      Initializes all the RTE buffers and update flags with start values.
 * \param      -
 * \return     Always E_OK.
 */
Std_ReturnType Rte_Start(void)
{
   /* Buffers. */
   Rte_Rfid_UserTag_Buf = 0UL;

   Rte_LedIn_LedGreen_Buf = RTE_LED_OFF;
   Rte_LedIn_LedYellow_Buf = RTE_LED_OFF;
   Rte_LedIn_LedRed_Buf = RTE_LED_OFF;
   Rte_LedIn_LedEngineGreen_Buf = RTE_LED_OFF;
   Rte_LedIn_LedEngineYellow_Buf = RTE_LED_OFF;
   Rte_LedIn_LedEngineRed_Buf = RTE_LED_OFF;

   Rte_ButtonOut_ButtonLeftSideMirrorLeft_Buf = RTE_BUTTON_NOT_PRESSED;
   Rte_ButtonOut_ButtonLeftSideMirrorRight_Buf = RTE_BUTTON_NOT_PRESSED;
   Rte_ButtonOut_ButtonRightSideMirrorLeft_Buf = RTE_BUTTON_NOT_PRESSED;
   Rte_ButtonOut_ButtonRightSideMirrorRight_Buf = RTE_BUTTON_NOT_PRESSED;
   Rte_ButtonOut_ButtonSteeringWheelUp_Buf = RTE_BUTTON_NOT_PRESSED;
   Rte_ButtonOut_ButtonSteeringWheelDown_Buf = RTE_BUTTON_NOT_PRESSED;
   Rte_ButtonOut_ButtonEngineOn_Buf = RTE_BUTTON_NOT_PRESSED;
   Rte_ButtonOut_ButtonEngineOff_Buf = RTE_BUTTON_NOT_PRESSED;

   Rte_ServoIn_ServoLeftSideMirror_Buf = 100000UL;
   Rte_ServoIn_ServoRightSideMirror_Buf = 100000UL;
   Rte_ServoIn_ServoSteeringWheel_Buf = 100000UL;

   Rte_MemoryRead_Read_Buf.t_Tag = 0UL;
   Rte_MemoryRead_Read_Buf.t_DutyLeft = 0U;
   Rte_MemoryRead_Read_Buf.t_DutyRight = 0U;
   Rte_MemoryRead_Read_Buf.t_DutyMiddle = 0U;

   Rte_MemoryWrite_Write_Buf.t_Tag = 0UL;
   Rte_MemoryWrite_Write_Buf.t_DutyLeft = 0U;
   Rte_MemoryWrite_Write_Buf.t_DutyRight = 0U;
   Rte_MemoryWrite_Write_Buf.t_DutyMiddle = 0U;

   /* Update flags. */
   Rte_Rfid_RfidIn_UserTag_UpdateFlag = FALSE;
   Rte_MemoryRead_MemoryOut_Read_UpdateFlag = FALSE;
   Rte_MemoryWrite_MemoryOut_Write_UpdateFlag = FALSE;

   return E_OK;
}
