/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Buffers.h
 *    \author     RTE Generator
 *    \brief      Generated file - shall not be manually edited. 
 *                Exports all the RTE buffers that are used in the read / write operations.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef RTE_BUFFERS_H
#define RTE_BUFFERS_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Rte_Type.h"
#include "IoHwAb.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/* Buffers. */
extern Rte_RfidType Rte_Rfid_UserTag_Buf;

extern Rte_LedCtrlType Rte_LedIn_LedGreen_Buf;
extern Rte_LedCtrlType Rte_LedIn_LedYellow_Buf;
extern Rte_LedCtrlType Rte_LedIn_LedRed_Buf;
extern Rte_LedCtrlType Rte_LedIn_LedEngineGreen_Buf;
extern Rte_LedCtrlType Rte_LedIn_LedEngineYellow_Buf;
extern Rte_LedCtrlType Rte_LedIn_LedEngineRed_Buf;

extern Rte_ButtonCtrlType Rte_ButtonOut_ButtonLeftSideMirrorLeft_Buf;
extern Rte_ButtonCtrlType Rte_ButtonOut_ButtonLeftSideMirrorRight_Buf;
extern Rte_ButtonCtrlType Rte_ButtonOut_ButtonRightSideMirrorLeft_Buf;
extern Rte_ButtonCtrlType Rte_ButtonOut_ButtonRightSideMirrorRight_Buf;
extern Rte_ButtonCtrlType Rte_ButtonOut_ButtonSteeringWheelUp_Buf;
extern Rte_ButtonCtrlType Rte_ButtonOut_ButtonSteeringWheelDown_Buf;
extern Rte_ButtonCtrlType Rte_ButtonOut_ButtonEngineOn_Buf;
extern Rte_ButtonCtrlType Rte_ButtonOut_ButtonEngineOff_Buf;

extern Rte_ServoDutyType Rte_ServoIn_ServoLeftSideMirror_Buf;
extern Rte_ServoDutyType Rte_ServoIn_ServoRightSideMirror_Buf;
extern Rte_ServoDutyType Rte_ServoIn_ServoSteeringWheel_Buf;

extern Rte_ParameterType Rte_MemoryRead_Read_Buf;

extern Rte_ParameterType Rte_MemoryWrite_Write_Buf;

/* Update flags. */
extern boolean Rte_Rfid_RfidIn_UserTag_UpdateFlag;
extern boolean Rte_MemoryRead_MemoryOut_Read_UpdateFlag;
extern boolean Rte_MemoryWrite_MemoryOut_Write_UpdateFlag;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* RTE_BUFFERS_H */