/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Button_Core.c
 *    \author     Alexandra Purcaru
 *    \brief      Implements the Button functions.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Button_Core.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the raw input values of the digital ECU signals that are read from RTE. */
IoHwAb_DigitalLevelType Button_gat_InputValues[BUTTON_NUMBER_OF_INSTANCES];

/** \brief  Defines the output values after processing the raw input values of the digital ECU signals that are written
 *          back to RTE. */
Rte_ButtonCtrlType Button_gat_OutputValues[BUTTON_NUMBER_OF_INSTANCES];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes all the variables that reads the on and off state of all of the buttons.
 * \param      -
 * \return     -
 */
void Button_gv_Init(void)
{
   uint8 uc_I;

   for (uc_I = 0U; uc_I < BUTTON_NUMBER_OF_INSTANCES; uc_I++)
   {
      Button_gat_InputValues[uc_I] = STD_LOW;
      Button_gat_OutputValues[uc_I] = RTE_BUTTON_NOT_PRESSED;
   }
}
/**
 * \brief      Implements the input signals according to the active value.
 * \param      -
 * \return     -
 */
void Button_gv_Main(void)
{
   uint8 uc_I;

    /* Read the digital input signals from RTE and store them in Button_gat_InputValues[]. */
    Button_gv_RteInputUpdate();

    /* Set the output values according the configured active values. */
    for (uc_I = 0U; uc_I < BUTTON_NUMBER_OF_INSTANCES; uc_I++)
    {
       if (Button_gat_InputValues[uc_I] == Button_gkat_ActiveValues[uc_I])
       {
          Button_gat_OutputValues[uc_I] = RTE_BUTTON_PRESSED;
       }
       else
       {
          Button_gat_OutputValues[uc_I] = RTE_BUTTON_NOT_PRESSED;
       }
    }

    /* Write the output values to the RTE buffers. */
    Button_gv_RteOutputUpdate();
}
