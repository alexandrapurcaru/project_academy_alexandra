/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Button.h
 *    \author     RTE Generator
 *    \brief      Generated file - shall not be manually edited. 
 *                Implements all the RTE read and write operations that are performed by the Button software
 *                component.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef RTE_BUTTON_H
#define RTE_BUTTON_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Rte.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#define Rte_Read_In_ButtonLeftSideMirrorLeft(x) ((*x) = IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_L_SIDE_MIRROR_LEFT))
#define Rte_Read_In_ButtonLeftSideMirrorRight(x) ((*x) = IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_L_SIDE_MIRROR_RIGHT))
#define Rte_Read_In_ButtonRightSideMirrorLeft(x) ((*x) = IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_R_SIDE_MIRROR_LEFT))
#define Rte_Read_In_ButtonRightSideMirrorRight(x) ((*x) = IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_R_SIDE_MIRROR_RIGHT))
#define Rte_Read_In_ButtonSteeringWheelUp(x) ((*x) = IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_STEERING_WHEEL_UP))
#define Rte_Read_In_ButtonSteeringWheelDown(x) ((*x) = IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_STEERING_WHEEL_DOWN))
#define Rte_Read_In_ButtonEngineOn(x) ((*x) = IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_ENGINE_ON))
#define Rte_Read_In_ButtonEngineOff(x) ((*x) = IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_ENGINE_OFF))

#define Rte_Write_Out_ButtonLeftSideMirrorLeft(x) (Rte_ButtonOut_ButtonLeftSideMirrorLeft_Buf = (*x))
#define Rte_Write_Out_ButtonLeftSideMirrorRight(x) (Rte_ButtonOut_ButtonLeftSideMirrorRight_Buf = (*x))
#define Rte_Write_Out_ButtonRightSideMirrorLeft(x) (Rte_ButtonOut_ButtonRightSideMirrorLeft_Buf = (*x))
#define Rte_Write_Out_ButtonRightSideMirrorRight(x) (Rte_ButtonOut_ButtonRightSideMirrorRight_Buf = (*x))
#define Rte_Write_Out_ButtonSteeringWheelUp(x) (Rte_ButtonOut_ButtonSteeringWheelUp_Buf = (*x))
#define Rte_Write_Out_ButtonSteeringWheelDown(x) (Rte_ButtonOut_ButtonSteeringWheelDown_Buf = (*x))
#define Rte_Write_Out_ButtonEngineOn(x) (Rte_ButtonOut_ButtonEngineOn_Buf = (*x))
#define Rte_Write_Out_ButtonEngineOff(x) (Rte_ButtonOut_ButtonEngineOff_Buf = (*x))

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* RTE_BUTTON_H */