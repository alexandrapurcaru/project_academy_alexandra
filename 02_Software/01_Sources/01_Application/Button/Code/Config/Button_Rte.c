/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Empty_Source.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Empty template to be used for all .c files. This file provides an example for the case in which a
 *                detailed description is not needed.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Button_Core.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Reads all the digital ECU signals and stores them to Btn_gat_InputValues[] for further processing
 *             in the SWC core.
 * \param      -
 * \return     -
 */
void Button_gv_RteInputUpdate(void)
{
   Rte_Read_In_ButtonLeftSideMirrorLeft(&Button_gat_InputValues[L_SIDE_MIRROR_LEFT]);
   Rte_Read_In_ButtonLeftSideMirrorRight(&Button_gat_InputValues[L_SIDE_MIRROR_RIGHT]);
   Rte_Read_In_ButtonRightSideMirrorLeft(&Button_gat_InputValues[R_SIDE_MIRROR_LEFT]);
   Rte_Read_In_ButtonRightSideMirrorRight(&Button_gat_InputValues[R_SIDE_MIRROR_RIGHT]);
   Rte_Read_In_ButtonSteeringWheelUp(&Button_gat_InputValues[STEERING_WHEEL_UP]);
   Rte_Read_In_ButtonSteeringWheelDown(&Button_gat_InputValues[STEERING_WHEEL_DOWN]);
   Rte_Read_In_ButtonEngineOn(&Button_gat_InputValues[ENGINE_ON]);
   Rte_Read_In_ButtonEngineOff(&Button_gat_InputValues[ENGINE_OFF]);
}

/**
 * \brief      Writes all the output RTE buffers with contents from the core processed Btn_gat_OutputValues[].
 * \param      -
 * \return     -
 */
void Button_gv_RteOutputUpdate(void)
{
   Rte_Write_Out_ButtonLeftSideMirrorLeft(&Button_gat_OutputValues[L_SIDE_MIRROR_LEFT]);
   Rte_Write_Out_ButtonLeftSideMirrorRight(&Button_gat_OutputValues[L_SIDE_MIRROR_RIGHT]);
   Rte_Write_Out_ButtonRightSideMirrorLeft(&Button_gat_OutputValues[R_SIDE_MIRROR_LEFT]);
   Rte_Write_Out_ButtonRightSideMirrorRight(&Button_gat_OutputValues[R_SIDE_MIRROR_RIGHT]);
   Rte_Write_Out_ButtonSteeringWheelUp(&Button_gat_OutputValues[STEERING_WHEEL_UP]);
   Rte_Write_Out_ButtonSteeringWheelDown(&Button_gat_OutputValues[STEERING_WHEEL_DOWN]);
   Rte_Write_Out_ButtonEngineOn(&Button_gat_OutputValues[ENGINE_ON]);
   Rte_Write_Out_ButtonEngineOff(&Button_gat_OutputValues[ENGINE_OFF]);
}
