/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Led.h
 *    \author     RTE Generator
 *    \brief      Generated file - shall not be manually edited. 
 *                Implements all the RTE read and write operations that are performed by the Led software
 *                component.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef RTE_LED_H
#define RTE_LED_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Rte.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#define Rte_Read_In_LedGreen(x) ((*x) = Rte_LedIn_LedGreen_Buf)
#define Rte_Read_In_LedYellow(x) ((*x) = Rte_LedIn_LedYellow_Buf)
#define Rte_Read_In_LedRed(x) ((*x) = Rte_LedIn_LedRed_Buf)
#define Rte_Read_In_LedEngineGreen(x) ((*x) = Rte_LedIn_LedEngineGreen_Buf)
#define Rte_Read_In_LedEngineYellow(x) ((*x) = Rte_LedIn_LedEngineYellow_Buf)
#define Rte_Read_In_LedEngineRed(x) ((*x) = Rte_LedIn_LedEngineRed_Buf)

#define Rte_Write_Out_LedGreen(x) (IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_LED_GREEN, (*x)))
#define Rte_Write_Out_LedYellow(x) (IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_LED_YELLOW, (*x)))
#define Rte_Write_Out_LedRed(x) (IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_LED_RED, (*x)))
#define Rte_Write_Out_LedEngineGreen(x) (IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_LED_GREEN_ENGINE, (*x)))
#define Rte_Write_Out_LedEngineYellow(x) (IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_LED_YELLOW_ENGINE, (*x)))
#define Rte_Write_Out_LedEngineRed(x) (IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_LED_RED_ENGINE, (*x)))

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* RTE_LED_H */