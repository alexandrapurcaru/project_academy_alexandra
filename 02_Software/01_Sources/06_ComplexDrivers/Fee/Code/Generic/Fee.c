/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Fee.c
 *    \author     Alexandra Purcaru
 *    \brief      Implements the FLS interfaces.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Fee.h"
#include "Std_Types.h"
#include "Fls.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the key 1 value for the key register. */
#define FEE_KEYR_KEY1                  (0x45670123)
/** \brief  Defines the key 2 value for the key register. */
#define FEE_KEYR_KEY2                  (0xCDEF89AB)
/** \brief  Defines the length of the data to be read. */
#define FEE_BUFFER_LENGTH                     (10U)

#define FEE_TIMEOUT_WRITE              (3U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

typedef enum
{
   FEE_STATE_LOCKED,
   FEE_STATE_UNLOCKED_ERASE,
   FEE_STATE_UNLOCKED_WRITE,
   FEE_STATE_CONFIGURATION,
   FEE_STATE_BUSY,
   FEE_STATE_ENDED,
} Fee_StateType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      The function makes a request of a write of a data in the flash memory.
 * \param      BlockNumber: Number of logical block.
 * \param      DataBufferPtr: Pointer to the data buffer.
 * \return     E_OK.
 */
Std_ReturnType Fee_gv_Write(uint16 BlockNumber, const uint8* DataBufferPtr)
{
   static Fee_StateType t_State = FEE_STATE_LOCKED;
   static boolean b_Write = FALSE;
   static boolean b_Erase = FALSE;
   static uint8 uc_Length = FEE_BUFFER_LENGTH;
   static Fls_AddressType t_MemoryAddress;
   static uint8* uc_Buffer;
   static Std_ReturnType t_Return = E_NOT_OK;
   static uint8 uc_NumberOfWrites = 0U;

   switch (t_State)
   {
      case (FEE_STATE_LOCKED):
      {
         FLASH->KEYR = FEE_KEYR_KEY1;
         FLASH->KEYR = FEE_KEYR_KEY2;

         FLASH->CR |= FLASH_CR_PSIZE_1;

         /* Prepares the flash memory for erase operation. */
         t_State = FEE_STATE_UNLOCKED_ERASE;

         t_Return = E_NOT_OK;
         break;
      }
      case (FEE_STATE_UNLOCKED_ERASE):
      {
         t_MemoryAddress = Fee_kat_BlockAddress[BlockNumber];
         uc_Buffer = (uint8*) *&DataBufferPtr;

         /*    Sets the sector erase. */
         FLASH->CR |= FLASH_CR_SER;
         /*    Sets the sector number that is going to be erased. Sets SNB. */
         FLASH->CR |= ((BlockNumber + 4) << 0x03);

         Fls_gv_Erase(Fee_kat_BlockAddress[BlockNumber], uc_Length);

         b_Erase = TRUE;

         t_State = FEE_STATE_BUSY;

         t_Return = E_NOT_OK;

         break;
      }
      case (FEE_STATE_UNLOCKED_WRITE):
      {
         Fls_gv_Write(t_MemoryAddress, (uint8*) *&uc_Buffer, uc_Length);

         t_MemoryAddress++;
         uc_Buffer++;
         uc_Length--;

         if (uc_Length == 0x00)
         {
            b_Write = TRUE;
         }

         t_State = FEE_STATE_BUSY;

         t_Return = E_NOT_OK;

         break;
      }
      case (FEE_STATE_CONFIGURATION):
      {
         if (b_Erase == TRUE)
         {
            /*    Clears the specific sector register. Clears SNB. */
            FLASH->CR &= ~((BlockNumber + 4) << 0x03);
            /*    Clears the sector erase register. */
            FLASH->CR &= ~(FLASH_CR_SER);

            FLASH->CR |= FLASH_CR_PG;

            b_Erase = FALSE;

            t_State = FEE_STATE_UNLOCKED_WRITE;
         }

         if (b_Write == TRUE)
         {
            uc_NumberOfWrites++;

            if (uc_NumberOfWrites < FEE_TIMEOUT_WRITE)
            {
               b_Write = FALSE;
               uc_Length = FEE_BUFFER_LENGTH;
               t_State = FEE_STATE_UNLOCKED_ERASE;
               t_Return = E_OK;
            }
            else
            {
               t_State = FEE_STATE_ENDED;
               FLASH->CR &= ~FLASH_CR_PG;
            }
         }
         else
         {

            t_State = FEE_STATE_UNLOCKED_WRITE;
         }

         break;
      }
      case (FEE_STATE_BUSY):
      {
         if ((FLASH->SR & FLASH_SR_BSY) == 0x00)
         {
            t_State = FEE_STATE_CONFIGURATION;
         }

         t_Return = E_NOT_OK;

         break;
      }
      case (FEE_STATE_ENDED):
      {
         FLASH->CR &= FLASH_CR_LOCK;

         t_State = FEE_STATE_LOCKED;

         uc_Length = FEE_BUFFER_LENGTH;

         t_Return = E_OK;

         break;
      }
   }

   return t_Return;
}

/**
 * \brief      The function requests a read in the Flash memory.
 * \param      BlockNumber: Number of logical block.
 * \param      BlockOffset: Read address offset inside the block.
 * \param      DataBufferPtr: Pointer to the data buffer.
 * \param      Length: Number of bytes to read.
 * \return     E_OK.
 */
Std_ReturnType Fee_gv_Read(uint16 BlockNumber, uint32 BlockOffset, uint8* DataBufferPtr, uint16 Length)
{
   if (Fee_kat_BlockAddress[BlockNumber] == BlockOffset)
   {
      Fls_gv_Read(BlockOffset, (uint8*) DataBufferPtr, Length);
   }

   return E_OK;
}
