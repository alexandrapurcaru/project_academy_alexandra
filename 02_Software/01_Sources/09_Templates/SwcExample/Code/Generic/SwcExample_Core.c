/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       SwcExample_Core.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Implements the behavior of the software component through generic runnables. The behavior can support
 *                multiple instances and the interaction with the RTE is performed through clearly defined input and
 *                output interfaces with configurable content.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "SwcExample_Core.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the raw input values of the digital ECU signals that are read from RTE. */
IoHwAb_DigitalLevelType SwcExample_gat_InputValues[SWCEXAMPLE_NUMBER_OF_INSTANCES];

/** \brief  Defines the output values after processing the raw input values of the digital ECU signals that are written
 *          back to RTE. */
Rte_BtnState SwcExample_gat_OutputValues[SWCEXAMPLE_NUMBER_OF_INSTANCES];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes all the core global variables.
 * \param      -
 * \return     -
 */
void SwcExample_Init(void)
{
   uint8 uc_I;

   for (uc_I = 0U; uc_I < SWCEXAMPLE_NUMBER_OF_INSTANCES; uc_I++)
   {
      SwcExample_gat_InputValues[uc_I] = STD_LOW;
      SwcExample_gat_OutputValues[uc_I] = RTE_BTN_UNPRESSED;
   }
}

/**
 * \brief      Implements the cyclic digital input signals processing according to configured active values.
 * \param      -
 * \return     -
 */
void SwcExample_MainFunction(void)
{
   uint8 uc_I;

   /* Read the digital input signals from RTE and store them in SwcExample_gat_InputValues[]. */
   SwcExample_gv_RteInputUpdate();

   /* Set the output values according the configured active values. */
   for (uc_I = 0U; uc_I < SWCEXAMPLE_NUMBER_OF_INSTANCES; uc_I++)
   {
      if (SwcExample_gat_InputValues[uc_I] == SwcExample_gkat_ActiveValues[uc_I])
      {
         SwcExample_gat_OutputValues[uc_I] = RTE_BTN_UNPRESSED;
      }
      else
      {
         SwcExample_gat_OutputValues[uc_I] = RTE_BTN_PRESSED;
      }
   }

   /* Write the output values to the RTE buffers. */
   SwcExample_gv_RteOutputUpdate();
}
