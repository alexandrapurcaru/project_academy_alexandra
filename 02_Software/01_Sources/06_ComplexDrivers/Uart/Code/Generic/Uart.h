/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Uart.h
 *    \author     Alexandra Purcaru
 *    \brief      Exports the data types and the interfaces used for implementations.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef UART_H
#define UART_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Uart_Cfg.h"
#include "Std_Types.h"
#include "stm32f407xx.h"
#include "RegInit.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief   Defines the base address configuration table. */
typedef struct
{
   /** \brief  Defines the base address of the UART peripheral. */
   USART_TypeDef* pt_BaseAddress;
} Uart_ChannelConfigType;

/** \brief  Defining the configuration table.*/
typedef struct
{
   /** \brief  Defines the pointer to the initialization registers. */
   const RegInit_Masked32BitsConfigType* kpt_Registers;
   /** \brief  Defines the pointer to the channel configuration registers. */
   const Uart_ChannelConfigType* kpt_Channels;
} Uart_ConfigType;

/** \brief  Defines the current status of the data transmitting interfaces. */
typedef enum
{
   /*    Defines the busy state of the transmission interfaces- the transmit buffer is not empty. */
   UART_TX_BUSY,
   /*    Defines the idle state of the transmission interfaces- the transmit buffer is empty and a new byte can be
    *    transmitted. */
   UART_TX_READY,
}Uart_TxStatusType;

/** \brief  Defines the current status of the data receiving interfaces. */
typedef enum
{
   /*    Defines the available state of the transmission interfaces- the receive buffer is empty. */
   UART_RX_NO_DATA,
   /*    Defines the completed state of the transmission interfaces- the receive buffer is full and the data is ready
    *    to be read. */
   UART_RX_READY,
}Uart_RxStatusType;

/** \brief  Defines the numeric ID of the UART transmit or receive channel. */
typedef uint8 Uart_ChannelType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const Uart_ConfigType Uart_gkt_Config;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void Uart_gv_Init(const Uart_ConfigType * pt_Config);
extern Uart_TxStatusType Uart_gt_GetTransmitStatus(Uart_ChannelType t_ChannelId);
extern Uart_RxStatusType Uart_gt_GetReceiveStatus(Uart_ChannelType t_ChannelId);
extern void Uart_gv_Transmit(Uart_ChannelType t_ChannelId, uint8 * puc_TransmitAddr);
extern void Uart_gv_Receive(Uart_ChannelType t_ChannelId, uint8 * puc_ReceiveAddr);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* UART_H */
