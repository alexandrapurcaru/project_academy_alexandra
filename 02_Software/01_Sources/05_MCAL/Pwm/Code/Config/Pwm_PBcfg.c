/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pwm_PBcfg.c
 *    \author     Alexandra Purcaru
 *    \brief      Defines the control registers list for setting the timers to the needed PWM periods.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Pwm.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the total number of configuration registers. */
#define PWM_LOCAL_NR_OF_REGISTERS (36U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the configuration registers array. */
static const RegInit_Masked32BitsSingleType Pwm_kat_Registers[PWM_LOCAL_NR_OF_REGISTERS] =
   {
      /*    For a clock frequency of 80MHz, set PSC = 0x20 then transform CLK/PSC in time period. The desired 50Hz,
       *       has 0.02 seconds(then transform in period of time). 0.02/((CLK/PSC)(in seconds)) = ARR. If ARR is greater
       *       than 65536, the PSC reset PSC to a value which will respect the condition. */
      /*    Sets the period necessary for 50 Hz frequency on the specific timer. */
      { & TIM3->ARR, ~(TIM_ARR_ARR), 50000 },
      /*    Sets the period necessary for 50 Hz frequency on the specific timer. */
      { & TIM3->ARR, ~(TIM_ARR_ARR), 50000 },
      /*    Sets the period necessary for 50 Hz frequency on the specific timer. */
      { & TIM4->ARR, ~(TIM_ARR_ARR), 50000 },
      /*    Sets the period necessary for 50 Hz frequency on the specific timer. */
      { & TIM4->ARR, ~(TIM_ARR_ARR), 50000 },

      { & TIM3->CCR4, ~(TIM_CCR4_CCR4), 50000 },
      { & TIM3->CCR2, ~(TIM_CCR2_CCR2), 50000 },
      { & TIM4->CCR1, ~(TIM_CCR1_CCR1), 50000 },
      { & TIM4->CCR2, ~(TIM_CCR2_CCR2), 50000 },

      /*    Set the counter as a down-counter for timer 2.*/
      { & TIM3->CR1, (uint16) ~(TIM_CR1_DIR), 0 },
      /*    Set the counter as a down-counter for timer 3.*/
      { & TIM3->CR1, (uint16) ~(TIM_CR1_DIR), 0 },
      /*    Set the counter as a down-counter for timer 4.*/
      { & TIM4->CR1, (uint16) ~(TIM_CR1_DIR), 0 },
      /*    Set the counter as a down-counter for timer 4.*/
      { & TIM4->CR1, (uint16) ~(TIM_CR1_DIR), 0 },

      /*    Generates an update of the registers for timer 2. */
      { & TIM3->EGR, (uint16) ~(TIM_EGR_UG), TIM_EGR_UG },
      /*    Generates an update of the registers for timer 3. */
      { & TIM3->EGR, (uint16) ~(TIM_EGR_UG), TIM_EGR_UG },
      /*    Generates an update of the registers for timer 4. */
      { & TIM4->EGR, (uint16) ~(TIM_EGR_UG), TIM_EGR_UG },
      /*    Generates an update of the registers for timer 4. */
      { & TIM4->EGR, (uint16) ~(TIM_EGR_UG), TIM_EGR_UG },

      /*    Enables the counter for timer 2. */
      { & TIM3->CR1, (uint16) ~(TIM_CR1_CEN), TIM_CR1_CEN },
      /*    Enables the counter for timer 3. */
      { & TIM3->CR1, (uint16) ~(TIM_CR1_CEN), TIM_CR1_CEN },
      /*    Enables the counter for timer 4. */
      { & TIM4->CR1, (uint16) ~(TIM_CR1_CEN), TIM_CR1_CEN },
      /*    Enables the counter for timer 4. */
      { & TIM4->CR1, (uint16) ~(TIM_CR1_CEN), TIM_CR1_CEN },

      /*    Sets the prescale value of the timer 2. */
      { & TIM3->PSC, (uint16) ~(TIM_PSC_PSC), 0x20 },
      /*    Sets the prescale value of the timer 3. */
      { & TIM3->PSC, (uint16) ~(TIM_PSC_PSC), 0x20 },
      /*    Sets the prescale value of the timer 4. */
      { & TIM4->PSC, (uint16) ~(TIM_PSC_PSC), 0x20 },
      /*    Sets the prescale value of the timer 4. */
      { & TIM4->PSC, (uint16) ~(TIM_PSC_PSC), 0x20 },

      /*    Clear the timer 2 status register*/
      { & TIM3->SR, (uint16) 0x00, 0x00 },
      /*    Clear the timer 3 status register*/
      { & TIM3->SR, (uint16) 0x00, 0x00 },
      /*    Clear the timer 4 status register*/
      { & TIM4->SR, (uint16) 0x00, 0x00 },
      /*    Clear the timer 4 status register*/
      { & TIM4->SR, (uint16) 0x00, 0x00 },

      /*    Sets timer 3 in PWM mode 1 and enables the output compare. */
      { & TIM3->CCMR2, ~( TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC4M_2 | TIM_CCMR2_OC4PE), TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC4M_2
         | TIM_CCMR2_OC4PE },
      /*    Sets timer 3 in PWM mode 1 and enables the output compare. */
      { & TIM3->CCMR1, ~( TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2PE), TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_2
         | TIM_CCMR1_OC2PE },
      /*    Sets timer 4 in PWM mode 1 and enables the output compare. */
      { & TIM4->CCMR1, ~( TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1PE), TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2
         | TIM_CCMR1_OC2PE },
      /*    Sets timer 4 in PWM mode 1 and enables the output compare. */
      { & TIM4->CCMR1, ~( TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2PE), TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_2
         | TIM_CCMR1_OC2PE },

      /*    Enables capture/compare output and sets the polarity to active low.*/
      { & TIM3->CCER, (uint16) ~(TIM_CCER_CC4E | TIM_CCER_CC4P), TIM_CCER_CC4E },
      /*    Enables capture/compare output and sets the polarity to active low.*/
      { & TIM3->CCER, (uint16) ~(TIM_CCER_CC2E | TIM_CCER_CC2P), TIM_CCER_CC2E },
      /*    Enables capture/compare output and sets the polarity to active low.*/
      { & TIM4->CCER, (uint16) ~(TIM_CCER_CC1E | TIM_CCER_CC1P), TIM_CCER_CC1E },
      /*    Enables capture/compare output and sets the polarity to active low.*/
      { & TIM4->CCER, (uint16) ~(TIM_CCER_CC2E | TIM_CCER_CC2P), TIM_CCER_CC2E },
   };

/** \brief  Defines channel configuration table. */
static const Pwm_ChannelConfigType Pwm_kat_Channels[PWM_NR_OF_CHANNELS] =
   {
      /*    Sets the capture/compare register and the auto-reload register.*/
      { (uint32*) & TIM3->CCR4, (uint32*) & TIM3->ARR },
      { (uint32*) & TIM3->CCR2, (uint32*) & TIM3->ARR },
      { (uint32*) & TIM4->CCR1, (uint32*) & TIM4->ARR },
      { (uint32*) & TIM4->CCR2, (uint32*) & TIM4->ARR },
   };

/** \brief  Defines the array containing the configuration registers and the number of registers. */
static const RegInit_Masked32BitsConfigType Pwm_kt_Config =
   { Pwm_kat_Registers, PWM_LOCAL_NR_OF_REGISTERS };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the structure containing the configuration registers, the channels to be used and the time period
 *          configuration registers for every timer. */
const Pwm_ConfigType Pwm_gkt_Config =
   { &Pwm_kt_Config, Pwm_kat_Channels };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

