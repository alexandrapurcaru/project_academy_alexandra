/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Dio.h
 *    \author     Alexandra Purcaru
 *    \brief      Exports the data types and the interfaces used for implementations.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef DIO_H
#define DIO_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "Dio_Cfg.h"
#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defining the registers table, creating the link between the base address and the necessary mask. */
typedef struct
{
   /** \brief  Defines the base address of the register. */
   GPIO_TypeDef* pt_BaseAdress;
   /** \brief  Defines the necessary mask for reading and writing a channel or a port. */
   uint16 us_Mask;
} Dio_ChannelConfigType;

/** \brief  Defines the data type for the numeric ID of a DIO port. */
typedef uint8 Dio_PortType;

/** \brief  Defines the data type for the numeric ID of a DIO channel. */
typedef uint8 Dio_ChannelType;

/** \brief  Defines the data type for the level of a DIO channel. */
typedef uint8 Dio_LevelType;

/** \brief  Defines the data type for the level of a DIO port. */
typedef uint16 Dio_PortLevelType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const Dio_ChannelConfigType Dio_gkat_ChannelConfig[DIO_NR_OF_CHANNELS];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern Dio_LevelType Dio_ReadChannel(Dio_ChannelType ChannelId);
extern void Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level);
extern Dio_PortLevelType Dio_ReadPort(Dio_PortType PortId);
extern void Dio_WritePort(Dio_PortType PortId, Dio_PortLevelType Level);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* DIO_H */
