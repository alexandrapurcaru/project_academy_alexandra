/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Buffers.c
 *    \author     RTE Generator
 *    \brief      Generated file - shall not be manually edited. 
 *                Declares all the RTE buffers that are used in the read / write operations.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Rte_Buffers.h"
#include "Rte.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/* Buffers. */
Rte_RfidType Rte_Rfid_UserTag_Buf;

Rte_LedCtrlType Rte_LedIn_LedGreen_Buf;
Rte_LedCtrlType Rte_LedIn_LedYellow_Buf;
Rte_LedCtrlType Rte_LedIn_LedRed_Buf;
Rte_LedCtrlType Rte_LedIn_LedEngineGreen_Buf;
Rte_LedCtrlType Rte_LedIn_LedEngineYellow_Buf;
Rte_LedCtrlType Rte_LedIn_LedEngineRed_Buf;

Rte_ButtonCtrlType Rte_ButtonOut_ButtonLeftSideMirrorLeft_Buf;
Rte_ButtonCtrlType Rte_ButtonOut_ButtonLeftSideMirrorRight_Buf;
Rte_ButtonCtrlType Rte_ButtonOut_ButtonRightSideMirrorLeft_Buf;
Rte_ButtonCtrlType Rte_ButtonOut_ButtonRightSideMirrorRight_Buf;
Rte_ButtonCtrlType Rte_ButtonOut_ButtonSteeringWheelUp_Buf;
Rte_ButtonCtrlType Rte_ButtonOut_ButtonSteeringWheelDown_Buf;
Rte_ButtonCtrlType Rte_ButtonOut_ButtonEngineOn_Buf;
Rte_ButtonCtrlType Rte_ButtonOut_ButtonEngineOff_Buf;

Rte_ServoDutyType Rte_ServoIn_ServoLeftSideMirror_Buf;
Rte_ServoDutyType Rte_ServoIn_ServoRightSideMirror_Buf;
Rte_ServoDutyType Rte_ServoIn_ServoSteeringWheel_Buf;

Rte_ParameterType Rte_MemoryRead_Read_Buf;

Rte_ParameterType Rte_MemoryWrite_Write_Buf;

/* Update flags. */
boolean Rte_Rfid_RfidIn_UserTag_UpdateFlag;
boolean Rte_MemoryRead_MemoryOut_Read_UpdateFlag;
boolean Rte_MemoryWrite_MemoryOut_Write_UpdateFlag;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

