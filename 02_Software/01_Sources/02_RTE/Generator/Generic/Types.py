import xlwings as xw
import os.path
from Common import *

class TypeDef:
	def __init__(self, name):
		self.name = name
		self.type = "TypeDef"
		self.base_type = ""
		self.used = 0
	def add_Member(self, base_type):
		self.base_type = base_type

class Enum:
	def __init__(self, name):
		self.name = name
		self.type = "Enum"
		self.member_name = []
		self.member_value = []
		self.used = 0
	def add_Member(self, member_name, member_value):
		self.member_name.append(member_name)
		self.member_value.append(member_value)
	def check_duplicate(self, name):
		if name in self.member_name:
			return 1
		else:
			return 0
	
class Struct:
	def __init__(self, name):
		self.name = name
		self.type = "Struct"
		self.member_type = []
		self.member_name = []
		self.used = 0
	def add_Member(self, member_type, member_name):
		self.member_type.append(member_type)
		self.member_name.append(member_name)
	def check_duplicate(self, name):
		if name in self.member_name:
			return 1
		else:
			return 0

class Type:
	def __init__(self):
		self.typedef = []
		self.enum = []
		self.struct = []
		self.sdtype = []
	def add_TypeDef(self, typedef):
		self.typedef.append(typedef)
	def add_Enum(self, enum):
		self.enum.append(enum)
	def add_Struct(self, struct):
		self.struct.append(struct)
	def add_SdType(self, sdtype):
		self.sdtype.append(sdtype)
	def check_duplicate(self, name):
		len_typedef = len(self.typedef)
		for i in range(0, len_typedef):
			if name == self.typedef[i].name:
				return 1
		
		len_enum = len(self.enum)
		for i in range(0, len_enum):
			if name == self.enum[i].name:
				return 1
		
		len_struct = len(self.struct)
		for i in range(0, len_struct):
			if name == self.struct[i].name:
				return 1
		
		return 0

data = Type()
log = Log()

def reader(sddata):
	wb = xw.Book(dirname + '/../Config/Types.xlsm')
	len_typedef = int(wb.sheets["TypeDef"].range(TOTAL_LINES_POS).value) + 1
	len_enum = int(wb.sheets["Enum"].range(TOTAL_LINES_POS).value) + 1
	len_struct = int(wb.sheets["Struct"].range(TOTAL_LINES_POS).value) + 1
	matrix_typedef = wb.sheets["TypeDef"].range(TYPEDEF_NEWT_POS + str(START_CELL_POS), TYPEDEF_BASET_POS + str(len_typedef)).value
	matrix_enum = wb.sheets["Enum"].range(ENUM_NEWT_POS + str(START_CELL_POS), ENUM_MEMBER_VAL_POS + str(len_enum)).value
	matrix_struct = wb.sheets["Struct"].range(STRUCT_NEWT_POS + str(START_CELL_POS), STRUCT_MEMBERN_POS + str(len_struct)).value
	wb.close()
	
	len_typedef = len_typedef - MINUS_VAL - 1
	len_enum = len_enum - MINUS_VAL - 1
	len_struct = len_struct - MINUS_VAL - 1
	
	error_found = 0
	#SDTYPE_READER
	len_sddata = len(sddata)
	for i in range(0, len_sddata):
		data.add_SdType(sddata[i])
	
	#TYPEDEF_READER
	for i in range(0, len_typedef):
		if matrix_typedef[i][0] == None or matrix_typedef[i][1] == None:
			log.errors = log.errors + "Invalid syntax in Types:TypeDef at the line " + str(i + MINUS_VAL + 1) + ".\n"
			error_found = 1
		
		if check_typename(matrix_typedef[i][0]) == 1:
			error_found = 1
			log.errors = log.errors + "Invalid name in Types:TypeDef at the line " + str(i + MINUS_VAL + 1) + ".(" + matrix_typedef[i][0] + ")\n"
		
		if data.check_duplicate(matrix_typedef[i][0]) == 1:
			log.errors = log.errors + "Types:Typedef name \"" + matrix_typedef[i][0] + "\" is duplicate.\n"
			error_found = 1
		
		typedef = TypeDef(matrix_typedef[i][0])
		
		found_type = 0
		len_sddata = len(sddata)
		for j in range(0, len_sddata):
			if sddata[j].name == matrix_typedef[i][1]:
				found_type = 1
				typedef.add_Member(sddata[j])
		
		if found_type == 0:
			log.errors = log.errors + "Type: " + matrix_typedef[i][0] + " from Types:TypeDef at the line " + str(i + MINUS_VAL + 1) + " is not standard!\n"
			error_found = 1
		
		data.add_TypeDef(typedef)

	#ENUM_READER
	for i in range(0, len_enum):
		if matrix_enum[i][1] == None:
			log.errors = log.errors + "Invalid syntax in Types:Enum at the line " + str(i + MINUS_VAL + 1) + ".\n"
			error_found = 1
		
		if matrix_enum[i][0] != None:
			if check_typename(matrix_enum[i][0]) == 1:
				log.errors = log.errors + "Invalid name in Types:Enum at the line " + str(i + MINUS_VAL + 1) + ".(" + matrix_enum[i][0] + ")\n"
				error_found = 1
			
			if data.check_duplicate(matrix_enum[i][0]) == 1:
				log.errors = log.errors + "Types:Enum name \"" + matrix_enum[i][0] + "\" is duplicate.\n"
				error_found = 1
			
			enum = Enum(matrix_enum[i][0])
			data.add_Enum(enum)
		
		if enum.check_duplicate(matrix_enum[i][1]) == 1:
			log.errors = log.errors + "Types:Enum:Member_name \"" + matrix_enum[i][1] + "\" is duplicate.\n"
			error_found = 1
		
		if check_enummember(matrix_enum[i][1]) == 1:
			log.errors = log.errors + "Types:Enum member name \"" + matrix_enum[i][1] + "\" is invalid.\n"
			error_found = 1
		else:
			enum.add_Member(matrix_enum[i][1], matrix_enum[i][2])
	
	#STRUCT_READER
	for i in range(0, len_struct):
		if matrix_struct[i][1] == None or matrix_struct[i][2] == None:
			log.errors = log.errors + "Invalid syntax in Types:Struct at the line " + str(i + MINUS_VAL + 1) + ".\n"
			error_found = 1
		
		if matrix_struct[i][0] != None:
			if check_typename(matrix_struct[i][0]) == 1:
				log.errors = log.errors + "Invalid name in Types:Struct at the line " + str(i + MINUS_VAL + 1) + ".(" + matrix_struct[i][0] + ")\n"
				error_found = 1
				
			if data.check_duplicate(matrix_struct[i][0]) == 1:
				log.errors = log.errors + "Types:Struct name \"" + matrix_struct[i][0] + "\" is duplicate.\n"
				error_found = 1
			
			struct = Struct(matrix_struct[i][0])
			data.add_Struct(struct)
		
		if struct.check_duplicate(matrix_struct[i][2]) == 1:
			log.errors = log.errors + "Types:Struct:Member_Name \"" + matrix_struct[i][2] + "\" is duplicate.\n"
			error_found = 1
		
		type_found = 0
		#duplicate
		length = len(sddata)
		for j in range(0, length):
			if data.sdtype[j].name == matrix_struct[i][1]:
				type_found = 1
				if (check_structprimmem(matrix_struct[i][1], matrix_struct[i][2]) == 1):
					log.errors = log.errors + "Types:Struct:Member_Name \"" + matrix_struct[i][2] + "\" is invalid.\n"
					error_found = 1
				else:
					struct.add_Member(data.sdtype[j], matrix_struct[i][2])
		
		length = len(data.typedef)
		for j in range(0, length):
			if data.typedef[j].name == matrix_struct[i][1]:
				type_found = 1
				data.typedef[j].used = 1
				if (check_structcustmem(matrix_struct[i][2]) == 1):
					log.errors = log.errors + "Types:Struct:Member_Name \"" + matrix_struct[i][2] + "\" is invalid.\n"
					error_found = 1
				else:
					struct.add_Member(data.typedef[j], matrix_struct[i][2])
		
		length = len(data.enum)
		for j in range(0, length):
			if data.enum[j].name == matrix_struct[i][1]:
				type_found = 1
				data.enum[j].used = 1
				if (check_structcustmem(matrix_struct[i][2]) == 1):
					log.errors = log.errors + "Types:Struct:Member_Name \"" + matrix_struct[i][2] + "\" is invalid.\n"
					error_found = 1
				else:
					struct.add_Member(data.enum[j], matrix_struct[i][2])
		
		length = len(data.struct) - 1
		for j in range(0, length):
			if data.struct[j].name == matrix_struct[i][1]:
				type_found = 1
				data.struct[j].used = 1
				if (check_structcustmem(matrix_struct[i][2]) == 1):
					log.errors = log.errors + "Types:Struct:Member_Name \"" + matrix_struct[i][2] + "\" is invalid.\n"
					error_found = 1
				else:
					struct.add_Member(data.struct[j], matrix_struct[i][2])
		
		#end_duplicate
		if type_found == 0:
			log.errors = log.errors + "Types:Struct:" + data.struct[len(data.struct) - 1].name + ":" + matrix_struct[i][1] + " not found!\n"
			error_found = 1
			
	if (0 == error_found):
		return 0
	else:
		return 1

def writer():
	type_h = open(dirname+"/../../../09_Templates/Empty/Code/Generic/Empty_Header.h", 'r')
	lines = type_h.readlines()
	type_h.close()
	type_h = open(dirname+"/../../Code/Config/Rte_Type.h", 'w')
	
	lines[H_FILE_N_POS] = " *    \\file       Rte_Type.h\n"
	lines[H_AUTHOR_POS] = " *    \\author     " + AUTHOR_N + "\n"
	lines[H_BRIEF_POS] = " *    \\brief      " + BRIEF + "\n"
	lines[H_BRIEF_POS + 1] = BRIEF_TYPES_H + "\n"
	lines[H_IFNDEF_POS] = "#ifndef RTE_TYPE_H\n"
	lines[H_DEF_POS] = "#define RTE_TYPE_H\n"
	lines[H_INCLUSIONS_POS] = "#include \"Std_Types.h\"\n"
	lines[H_ENDIF_POS] = "#endif /* RTE_TYPE_H */"
	
	#TYPEDEF_WRITER
	len_typedef = len(data.typedef)
	for count in range(0, len_typedef):
		lines[H_G_DTYPE_POS] = lines[H_G_DTYPE_POS] + "typedef " + data.typedef[count].base_type.name + " " + data.typedef[count].name + ";\n"
	
	lines[H_G_DTYPE_POS] = lines[H_G_DTYPE_POS] + "\n"
	
	#ENUM_WRITER
	len_enum = len(data.enum)
	for count in range(0, len_enum):
		lines[H_G_DTYPE_POS] = lines[H_G_DTYPE_POS] + "typedef enum\n{\n"
		
		len_enum_member_name = len(data.enum[count].member_name)
		for i in range(0, len_enum_member_name):
			lines[H_G_DTYPE_POS] = lines[H_G_DTYPE_POS] + "   " + data.enum[count].member_name[i]
			if data.enum[count].member_value[i] != None:
				lines[H_G_DTYPE_POS] = lines[H_G_DTYPE_POS] + " = " + data.enum[count].member_value[i]
			
			lines[H_G_DTYPE_POS] = lines[H_G_DTYPE_POS] + ",\n"
		lines[H_G_DTYPE_POS] = lines[H_G_DTYPE_POS] + "} " + data.enum[count].name + ";\n\n"
	
	#STRUCT_WRITER
	len_struct = len(data.struct)
	for count in range(0, len_struct):
		lines[H_G_DTYPE_POS] = lines[H_G_DTYPE_POS] + "typedef struct\n{\n"
		
		len_struct_member_type = len(data.struct[count].member_type)
		for i in range(0, len_struct_member_type):
			lines[H_G_DTYPE_POS] = lines[H_G_DTYPE_POS] + "   " + data.struct[count].member_type[i].name + " " + data.struct[count].member_name[i] + ";\n"
		
		lines[H_G_DTYPE_POS] = lines[H_G_DTYPE_POS] + "} " + data.struct[count].name + ";\n\n"
		
	
	lines[H_G_DTYPE_POS] = lines[H_G_DTYPE_POS] + "\n"
	
	type_h.writelines(lines)
	type_h.close()
	
	return 0

def check():
	#duplicate
	len_typedef = len(data.typedef)
	for i in range(0, len_typedef):
		if data.typedef[i].used == 0:
			log.warnings = log.warnings + "Typedef \"" + data.typedef[i].name + "\" is not used.\n"
			
	len_enum = len(data.enum)
	for i in range(0, len_enum):
		if data.enum[i].used == 0:
			log.warnings = log.warnings + "Enum \"" + data.enum[i].name + "\" is not used.\n"
	
	len_struct = len(data.struct)
	for i in range(0, len_struct):
		if data.struct[i].used == 0:
			log.warnings = log.warnings + "Struct \"" + data.struct[i].name + "\" is not used.\n"
	#end_duplicate
	
	return 0