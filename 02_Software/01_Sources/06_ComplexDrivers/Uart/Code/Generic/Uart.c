/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Uart.c
 *    \author     Alexandra Purcaru
 *    \brief      Implements the UART interfaces.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Uart.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defining the configuration address of the UART channels. */
static Uart_ChannelConfigType* Uart_pt_Channels;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      The function initializes the control registers for the UART configured peripherals.
 * \param      pt_Config: Pointer to the configuration set.
 * \return     -
 */
void Uart_gv_Init(const Uart_ConfigType * pt_Config)
{
   Uart_pt_Channels = (Uart_ChannelConfigType*) pt_Config->kpt_Channels;

   RegInit_gv_Masked32Bits(pt_Config -> kpt_Registers);
}

#if (UART_TRANSMIT_API == STD_ON)

/**
 * \brief      The function verifies the transmission status of the specified channel.
 * \param      t_ChannelId: Numeric ID of the UART channel.
 * \return     The transmission status: UART_TX_READY if the transmission is complete, UART_TX_BUSY if the the
 *             transmission is not completed yet.
 */
Uart_TxStatusType Uart_gt_GetTransmitStatus(Uart_ChannelType t_ChannelId)
{
   Uart_TxStatusType t_Status = UART_TX_BUSY;

   /*    Checks if transmit data register is full. */
   if ((Uart_pt_Channels[t_ChannelId].pt_BaseAddress->SR & USART_SR_TXE) != 0x00)
   {
      /*    Checks if transmission is complete. */
      if ((Uart_pt_Channels[t_ChannelId].pt_BaseAddress->SR & USART_SR_TC) != 0x00)
      {
         t_Status = UART_TX_READY;
      }
      else
      {
         t_Status = UART_TX_BUSY;
      }
   }
   else
   {
      t_Status = UART_TX_BUSY;
   }

   return t_Status;
}

/**
 * \brief      The function transmits data from a given address to the data register.
 * \param      t_ChannelId: Numeric ID of the UART channel.
 * \param      puc_TransmitAddr: Pointer to the address of the data that is going to be transmitted.
 * \return     -
 */
void Uart_gv_Transmit(Uart_ChannelType t_ChannelId, uint8 * puc_TransmitAddr)
{
   /*    Copy the value from transmission buffer address to transmission data register. */
   Uart_pt_Channels[t_ChannelId].pt_BaseAddress->DR = *puc_TransmitAddr;
}

#endif

#if (UART_RECEIVE_API == STD_ON)

/**
 * \brief      The function verifies the receiving status of the specified channel.
 * \param      t_ChannelId: Numeric ID of the UART channel.
 * \return     The receiving status: UART_RX_READY if the data has been received, UART_RX_NO_DATA if the data has not
 *             been received.
 */
Uart_RxStatusType Uart_gt_GetReceiveStatus(Uart_ChannelType t_ChannelId)
{
   Uart_RxStatusType t_Status;

   /*    Checks if the read data register is full. */
   if ((Uart_pt_Channels[t_ChannelId].pt_BaseAddress->SR & USART_SR_RXNE) != 0x00)
   {
      t_Status = UART_RX_READY;
   }
   else
   {
      t_Status = UART_RX_NO_DATA;
   }
   return t_Status;
}

/**
 * \brief      The function receives data from the data register and writes it to a given address.
 * \param      t_ChannelId: Numeric ID of the UART channel.
 * \param      puc_ReceiveAddr: Pointer to the address where the received data is going to be written.
 * \return     -
 */
void Uart_gv_Receive(Uart_ChannelType t_ChannelId, uint8 * puc_ReceiveAddr)
{
   /*    Copy the value from receive data register to receive data buffer address. */
   *puc_ReceiveAddr = Uart_pt_Channels[t_ChannelId].pt_BaseAddress->DR;
}

#endif
