/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Os_Interrupts_Cfg.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Performs initializations of all interrupts and defines the interrupt vector which contains: the
 *                stack pointer, reset handler, exception handles and interrupt handles.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Os_Interrupts_Cfg.h"
#include "Os_Measurements.h"
#include "Linker_Symbols.h"
#include "EcuM.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the interface of all the handlers inside the interrupt vector. */
typedef void (* const Os_InterruptsHandler)(void);

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the list of all the interrupt routines. It has an explicit section name and relies on the linker
 *          script for being placed at the correct location in memory. */
__attribute__ ((section(".isr_vector"), used)) const Os_InterruptsHandler Os_kapf_InterruptVector[] =
   {
      /* ----------------------------------------------------------------------------------- */
      /* Cortex-M core handlers. */
      (Os_InterruptsHandler) &_Linker_StackHead_, /*   The stack pointer                     */
      EcuM_Init, /*                                    The reset handler                     */
      Os_gv_ExceptionHandler, /*                       The NMI handler                       */
      Os_gv_ExceptionHandler, /*                       The hard fault handler                */
      Os_gv_ExceptionHandler, /*                       The MPU fault handler                 */
      Os_gv_ExceptionHandler, /*                       The bus fault handler                 */
      Os_gv_ExceptionHandler, /*                       The usage fault handler               */
      NULL_PTR, /*                                     Reserved                              */
      NULL_PTR, /*                                     Reserved                              */
      NULL_PTR, /*                                     Reserved                              */
      NULL_PTR, /*                                     Reserved                              */
      Os_gv_ExceptionHandler, /*                       SVCall handler                        */
      Os_gv_ExceptionHandler, /*                       Debug monitor handler                 */
      NULL_PTR, /*                                     Reserved                              */
      Os_gv_ExceptionHandler, /*                       The PendSV handler                    */
      Os_gv_ExceptionHandler, /*                       The SysTick handler                   */
      /* ----------------------------------------------------------------------------------- */
      /* External interrupt handlers. */
      NULL_PTR, /*                                     Window WatchDog                       */
      NULL_PTR, /*                                     PVD - EXTI Line                       */
      NULL_PTR, /*                                     Tamper and TimeStamps - EXTI line     */
      NULL_PTR, /*                                     RTC wake-up - EXTI line               */
      NULL_PTR, /*                                     FLASH                                 */
      NULL_PTR, /*                                     RCC                                   */
      NULL_PTR, /*                                     EXTI Line0                            */
      NULL_PTR, /*                                     EXTI Line1                            */
      NULL_PTR, /*                                     EXTI Line2                            */
      NULL_PTR, /*                                     EXTI Line3                            */
      NULL_PTR, /*                                     EXTI Line4                            */
      NULL_PTR, /*                                     DMA1 Stream 0                         */
      NULL_PTR, /*                                     DMA1 Stream 1                         */
      NULL_PTR, /*                                     DMA1 Stream 2                         */
      NULL_PTR, /*                                     DMA1 Stream 3                         */
      NULL_PTR, /*                                     DMA1 Stream 4                         */
      NULL_PTR, /*                                     DMA1 Stream 5                         */
      NULL_PTR, /*                                     DMA1 Stream 6                         */
      NULL_PTR, /*                                     ADC1, ADC2 and ADC3s                  */
      NULL_PTR, /*                                     CAN1 TX                               */
      NULL_PTR, /*                                     CAN1 RX0                              */
      NULL_PTR, /*                                     CAN1 RX1                              */
      NULL_PTR, /*                                     CAN1 SCE                              */
      NULL_PTR, /*                                     External Line[9:5]s                   */
      NULL_PTR, /*                                     TIM1 Break and TIM9                   */
      NULL_PTR, /*                                     TIM1 Update and TIM10                 */
      NULL_PTR, /*                                     TIM11, TIM1 Trigger and commutation   */
      NULL_PTR, /*                                     TIM1 Capture Compare                  */
      NULL_PTR, /*                                     TIM2                                  */
      NULL_PTR, /*                                     TIM3                                  */
      NULL_PTR, /*                                     TIM4                                  */
      NULL_PTR, /*                                     I2C1 Event                            */
      NULL_PTR, /*                                     I2C1 Error                            */
      NULL_PTR, /*                                     I2C2 Event                            */
      NULL_PTR, /*                                     I2C2 Error                            */
      NULL_PTR, /*                                     SPI1                                  */
      NULL_PTR, /*                                     SPI2                                  */
      NULL_PTR, /*                                     USART1                                */
      NULL_PTR, /*                                     USART2                                */
      NULL_PTR, /*                                     USART3                                */
      NULL_PTR, /*                                     External Line[15:10]s                 */
      NULL_PTR, /*                                     RTC Alarm (A and B) through EXTI Line */
      NULL_PTR, /*                                     USB OTG FS wake-up through EXTI line  */
      NULL_PTR, /*                                     TIM8 Break and TIM12                  */
      NULL_PTR, /*                                     TIM8 Update and TIM13                 */
      NULL_PTR, /*                                     TIM14, TIM8 Trigger and commutation   */
      NULL_PTR, /*                                     TIM8 Capture Compare                  */
      NULL_PTR, /*                                     DMA1 Stream7                          */
      NULL_PTR, /*                                     FMC                                   */
      NULL_PTR, /*                                     SDIO                                  */
      NULL_PTR, /*                                     TIM5                                  */
      NULL_PTR, /*                                     SPI3                                  */
      NULL_PTR, /*                                     UART4                                 */
      NULL_PTR, /*                                     UART5                                 */
      NULL_PTR, /*                                     TIM6 and DAC1&2 under-run errors      */
      NULL_PTR, /*                                     TIM7                                  */
      NULL_PTR, /*                                     DMA2 Stream 0                         */
      NULL_PTR, /*                                     DMA2 Stream 1                         */
      NULL_PTR, /*                                     DMA2 Stream 2                         */
      NULL_PTR, /*                                     DMA2 Stream 3                         */
      NULL_PTR, /*                                     DMA2 Stream 4                         */
      NULL_PTR, /*                                     Ethernet                              */
      NULL_PTR, /*                                     Ethernet wake-up through EXTI line    */
      NULL_PTR, /*                                     CAN2 TX                               */
      NULL_PTR, /*                                     CAN2 RX0                              */
      NULL_PTR, /*                                     CAN2 RX1                              */
      NULL_PTR, /*                                     CAN2 SCE                              */
      NULL_PTR, /*                                     USB OTG FS                            */
      NULL_PTR, /*                                     DMA2 Stream 5                         */
      NULL_PTR, /*                                     DMA2 Stream 6                         */
      NULL_PTR, /*                                     DMA2 Stream 7                         */
      NULL_PTR, /*                                     USART6                                */
      NULL_PTR, /*                                     I2C3 event                            */
      NULL_PTR, /*                                     I2C3 error                            */
      NULL_PTR, /*                                     USB OTG HS End Point 1 Out            */
      NULL_PTR, /*                                     USB OTG HS End Point 1 In             */
      NULL_PTR, /*                                     USB OTG HS wake-up through EXTI       */
      NULL_PTR, /*                                     USB OTG HS                            */
      NULL_PTR, /*                                     DCMI                                  */
      NULL_PTR, /*                                     Reserved                              */
      NULL_PTR, /*                                     Hash and Rng                          */
      NULL_PTR, /*                                     FPU                                   */
   };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Called when an unexpected interrupt occurs or a specific handler is not present in the application code.
 * \param      -
 * \return     -
 */
void __attribute__ ((section(".after_vectors"))) Os_gv_ExceptionHandler(void)
{
   /* Enter infinite loop and wait for a watchdog reset (if available). */
   while (TRUE)
   {
   }
}
