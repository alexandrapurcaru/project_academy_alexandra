/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Port_PBcfg.c
 *    \author     Alexandra Purcaru
 *    \brief      Defines the mode registers and direction registers to be loaded in Port_Init().
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Port.h"
#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the total number of registers. */
#define PORT_LOCAL_NR_OF_REGISTERS (50U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the mode and direction registers array. */
static const RegInit_Masked32BitsSingleType Port_kat_Registers[PORT_LOCAL_NR_OF_REGISTERS] =
   {
      /* Configuration for DIO channels. */

      /*    Sets pin 0 of port D in input mode. */
      { & GPIOD->MODER, (uint32) ~(GPIO_MODER_MODER0_0), (0x00) },
      /*    Sets pin 0 of port D in input mode. */
      { & GPIOD->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR0), (GPIO_OSPEEDER_OSPEEDR0) },
      /*    Sets pin 1 of port D in input mode. */
      { & GPIOD->MODER, (uint32) ~(GPIO_MODER_MODER1_0), (0x00) },
      /*     Sets the very high speed for pin 8 of port D. */
      { & GPIOD->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR1), (GPIO_OSPEEDER_OSPEEDR1) },
      /*    Sets pin 2 of port D in input mode. */
      { & GPIOD->MODER, (uint32) ~(GPIO_MODER_MODER2_0), (0x00) },
      /*     Sets the very high speed for pin 9 of port D. */
      { & GPIOD->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR2), (GPIO_OSPEEDER_OSPEEDR2) },
      /*    Sets pin 3 of port D in input mode. */
      { & GPIOD->MODER, (uint32) ~(GPIO_MODER_MODER3_0), (0x00) },
      /*     Sets the very high speed for pin 10 of port D. */
      { & GPIOD->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR3), (GPIO_OSPEEDER_OSPEEDR3) },
      /*    Sets pin 4 of port D in input mode. */
      { & GPIOD->MODER, (uint32) ~(GPIO_MODER_MODER4_0), (0x00) },
      /*     Sets the very high speed for pin 11 of port D. */
      { & GPIOD->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR4), (GPIO_OSPEEDER_OSPEEDR4) },
      /*    Sets pin 8 of port D in input mode. */
      { & GPIOD->MODER, (uint32) ~(GPIO_MODER_MODER8_0), (0x00) },
      /*     Sets the very high speed for pin 12 of port D. */
      { & GPIOD->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR8), (GPIO_OSPEEDER_OSPEEDR8) },
      /*    Sets pin 9 of port D in input mode. */
      { & GPIOD->MODER, (uint32) ~(GPIO_MODER_MODER9_0), (0x00) },
      /*     Sets the very high speed for pin 12 of port D. */
      { & GPIOD->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR9), (GPIO_OSPEEDER_OSPEEDR9) },
      /*    Sets pin 10 of port D in input mode. */
      { & GPIOD->MODER, (uint32) ~(GPIO_MODER_MODER10_0), (0x00) },
      /*     Sets the very high speed for pin 12 of port D. */
      { & GPIOD->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR10), (GPIO_OSPEEDER_OSPEEDR10) },

      /*    Sets pin 6 of port E in input mode. */
      { & GPIOE->MODER, (uint32) ~(GPIO_MODER_MODER6_0), (0x00) },
      /*     Sets the very high speed for pin 6 of port E. */
      { & GPIOE->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR6), (GPIO_OSPEEDER_OSPEEDR6) },
      /*    Sets pin 7 of port E in input mode. */
      { & GPIOE->MODER, (uint32) ~(GPIO_MODER_MODER7_0), (0x00) },
      /*     Sets the very high speed for pin 7 of port E. */
      { & GPIOE->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR7), (GPIO_OSPEEDER_OSPEEDR7) },

      /*    Sets pin 0 of port E in output mode. */
      { & GPIOE->MODER, (uint32) ~(GPIO_MODER_MODER0_0), (GPIO_MODER_MODER0_0) },
      /*     Sets the very high speed for pin 0 of port E. */
      { & GPIOE->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR0), (GPIO_OSPEEDER_OSPEEDR0) },
      /*    Sets pin 1 of port E in output mode. */
      { & GPIOE->MODER, (uint32) ~(GPIO_MODER_MODER1_0), (GPIO_MODER_MODER1_0) },
      /*     Sets the very high speed for pin 1 of port E. */
      { & GPIOE->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR1), (GPIO_OSPEEDER_OSPEEDR1) },
      /*    Sets pin 2 of port E in output mode. */
      { & GPIOE->MODER, (uint32) ~(GPIO_MODER_MODER2_0), (GPIO_MODER_MODER2_0) },
      /*     Sets the very high speed for pin 2 of port E. */
      { & GPIOE->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR2), (GPIO_OSPEEDER_OSPEEDR2) },
      /*    Sets pin 3 of port E in output mode. */
      { & GPIOE->MODER, (uint32) ~(GPIO_MODER_MODER3_0), (GPIO_MODER_MODER3_0) },
      /*     Sets the very high speed for pin 3 of port E. */
      { & GPIOE->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR3), (GPIO_OSPEEDER_OSPEEDR3) },
      /*    Sets pin 4 of port E in output mode. */
      { & GPIOE->MODER, (uint32) ~(GPIO_MODER_MODER4_0), (GPIO_MODER_MODER4_0) },
      /*     Sets the very high speed for pin 4 of port E. */
      { & GPIOE->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR4), (GPIO_OSPEEDER_OSPEEDR4) },
      /*    Sets pin 5 of port E in output mode. */
      { & GPIOE->MODER, (uint32) ~(GPIO_MODER_MODER5_0), (GPIO_MODER_MODER5_0) },
      /*     Sets the very high speed for pin 5 of port E. */
      { & GPIOE->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR5), (GPIO_OSPEEDER_OSPEEDR5) },

      /* Configuration for PWM channels. */

      /*    Sets pin 1 of port B in alternate function mode. */
      { & GPIOB->MODER, (uint32) ~(GPIO_MODER_MODER1_1), (GPIO_MODER_MODER1_1) },
      /*     Sets the very high speed for pin 3 of port A. */
      { & GPIOB->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR3), (GPIO_OSPEEDER_OSPEEDR3) },

      /*    Sets pin 5 of port B in alternate function mode. */
      { & GPIOB->MODER, (uint32) ~(GPIO_MODER_MODER5_1), (GPIO_MODER_MODER5_1) },
      /*     Sets the very high speed for pin 5 of port B. */
      { & GPIOB->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR5), (GPIO_OSPEEDER_OSPEEDR5) },

      /*    Sets pin 7 of port B in alternate function mode. */
      { & GPIOB->MODER, (uint32) ~(GPIO_MODER_MODER7_1), (GPIO_MODER_MODER7_1) },
      /*     Sets the very high speed for pin 7 of port B. */
      { & GPIOB->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR7), (GPIO_OSPEEDER_OSPEEDR7) },

      /*    Sets pin 6 of port B in alternate function mode. */
      { & GPIOB->MODER, (uint32) ~(GPIO_MODER_MODER6_1), (GPIO_MODER_MODER6_1) },
      /*     Sets the very high speed for pin 6 of port B. */
      { & GPIOB->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR6), (GPIO_OSPEEDER_OSPEEDR6) },

      /*    Sets pin 1 of port B in AFR register to AF2. */
      { & GPIOB->AFRL, (uint32) ~(GPIO_AFRL_AFRL1_1), GPIO_AFRL_AFRL1_1 },
      /*    Sets pin 5 of port B in AFR register to AF2. */
      { & GPIOB->AFRL, (uint32) ~(GPIO_AFRL_AFRL5_1), GPIO_AFRL_AFRL5_1 },
      /*    Sets pin 7 of port B in AFR register to AF2. */
      { & GPIOB->AFRL, (uint32) ~(GPIO_AFRL_AFRL7_1), GPIO_AFRL_AFRL7_1 },
      /*    Sets pin 6 of port B in AFR register to AF2. */
      { & GPIOB->AFRL, (uint32) ~(GPIO_AFRL_AFRL6_1), GPIO_AFRL_AFRL6_1 },

      /* Configuration for UART channels. */

      /*     Sets the very high speed for pin 10 of port B. */
      { & GPIOB->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR3), (GPIO_OSPEEDER_OSPEEDR3) },
      /*    Sets pin 10 of port B in AFR register to AF7. */
      { & GPIOB->AFRH, (uint32) ~(GPIO_AFRH_AFRH10_0 | GPIO_AFRH_AFRH10_1 | GPIO_AFRH_AFRH10_2), (GPIO_AFRH_AFRH10_0
         | GPIO_AFRH_AFRH10_1 | GPIO_AFRH_AFRH10_2) },
      /*    Sets pin 10 of port B in alternate function mode. */
      { & GPIOB->MODER, (uint32) ~(GPIO_MODER_MODER10_1), (GPIO_MODER_MODER10_1) },

      /*     Sets the very high speed for pin 11 of port B. */
      { & GPIOB->OSPEEDR, (uint32) ~(GPIO_OSPEEDER_OSPEEDR3), (GPIO_OSPEEDER_OSPEEDR3) },
      /*    Sets pin 11 of port B in AFR register to AF7. */
      { & GPIOB->AFRH, (uint32) ~(GPIO_AFRH_AFRH11_0 | GPIO_AFRH_AFRH11_1 | GPIO_AFRH_AFRH11_2), (GPIO_AFRH_AFRH11_0
         | GPIO_AFRH_AFRH11_1 | GPIO_AFRH_AFRH11_2) },
      /*    Sets pin 11 of port B in alternate function mode. */
      { & GPIOB->MODER, (uint32) ~(GPIO_MODER_MODER11_1), (GPIO_MODER_MODER11_1) },
   };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the array containing the registers and the number of them. */
const Port_ConfigType Port_gkt_Config =
   { Port_kat_Registers, PORT_LOCAL_NR_OF_REGISTERS };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

