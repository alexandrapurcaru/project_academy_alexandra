/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Empty_Source.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Empty template to be used for all .c files. This file provides an example for the case in which a
 *                detailed description is not needed.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Led_Core.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Reads all the LEDs control data and stores it to Led_gat_InputValues[] for further processing
 *             in the SWC core.
 * \param      -
 * \return     -
 */
void Led_gv_RteInputUpdate(void)
{
   Rte_Read_In_LedGreen(&Led_gat_InputValues[LED_GREEN]);
   Rte_Read_In_LedYellow(&Led_gat_InputValues[LED_YELLOW]);
   Rte_Read_In_LedRed(&Led_gat_InputValues[LED_RED]);
   Rte_Read_In_LedEngineGreen(&Led_gat_InputValues[LED_ENGINE_GREEN]);
   Rte_Read_In_LedEngineYellow(&Led_gat_InputValues[LED_ENGINE_YELLOW]);
   Rte_Read_In_LedEngineRed(&Led_gat_InputValues[LED_ENGINE_RED]);
}

/**
 * \brief      Writes all the PWM channels with the processed duty cycles from Led_gat_OutputValues[].
 * \param      -
 * \return     -
 */
void Led_gv_RteOutputUpdate(void)
{
   Rte_Write_Out_LedGreen(&Led_gat_OutputValues[LED_GREEN]);
   Rte_Write_Out_LedYellow(&Led_gat_OutputValues[LED_YELLOW]);
   Rte_Write_Out_LedRed(&Led_gat_OutputValues[LED_RED]);
   Rte_Write_Out_LedEngineGreen(&Led_gat_OutputValues[LED_ENGINE_GREEN]);
   Rte_Write_Out_LedEngineYellow(&Led_gat_OutputValues[LED_ENGINE_YELLOW]);
   Rte_Write_Out_LedEngineRed(&Led_gat_OutputValues[LED_ENGINE_RED]);
}
