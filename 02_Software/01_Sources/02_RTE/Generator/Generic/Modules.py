import xlwings as xw
import os.path
from Common import *

class Runnable:
	def __init__(self, name):
		self.name = name
		self.used = 0

class Module:
	def __init__(self, name):
		self.name = name
		self.port_name = []
		self.port_type = []
		self.used_interface = []
		self.runnable = []
	def add_Member(self, port_name, port_type, used_interface):
		self.port_name.append(port_name)
		self.port_type.append(port_type)
		self.used_interface.append(used_interface)
	def add_Runnable(self, runnable):
		self.runnable.append(runnable)
	def check_RunnableDuplicate(self, name):
		len_runnable = len(self.runnable)
		for i in range(0, len_runnable):
			if name == self.runnable[i].name:
				return 1
			
		return 0
	def check_duplicate(self, name):
		if name in self.port_name:
			return 1
		else:
			return 0

class Application:
	def __init__(self):
		self.module = []
	def add_Module(self, module):
		self.module.append(module)
		
class Recursive:
	def __init__(self):
		self.log = ""
		self.count = 0
	def reset(self):
		self.log = ""
		self.count = 0

data = Application()
log = Log()
recursive_func = Recursive()

def reader(interfaces):
	wb_main = xw.Book(dirname + "/../Config/Main.xlsm")
	len_main = int(wb_main.sheets["Main"].range(TOTAL_LINES_POS).value)
	array_main = wb_main.sheets["Main"].range(MAIN_MODULE_COL_POS + str(START_CELL_POS), MAIN_MODULE_COL_POS + str(len_main + 1)).value
	len_main = len_main - MINUS_VAL
	
	if len_main != 0:
		for i in range(0, len_main):
			module = Module(array_main[i])
			wb_module = xw.Book(dirname + "/../Config/Modules/" + array_main[i] + '.xlsm')
			len_module = int(wb_module.sheets[0].range(TOTAL_LINES_POS).value)
			len_module_run = int(wb_module.sheets[0].range(TOTAL_LINES_RUN_POS).value)
			matrix_module = wb_module.sheets[0].range(MODULE_PORT_N_POS + str(START_CELL_POS), MODULE_USEDINTERF_POS + str(len_module + 1)).value
			array_module = wb_module.sheets[0].range(MODULE_RUNNABLE_POS + str(START_CELL_POS), MODULE_RUNNABLE_POS + str(len_module_run + 1)).value
			wb_module.close()
			
			len_module = len_module - MINUS_VAL
			len_module_run = len_module_run - MINUS_VAL
			if ((len_module == 0) and (len_module_run == 0)):
				log.errors = log.errors + "Module \"" + array_main[i] + "\" is empty!\n"
				return 1
			
			for j in range(0, len_module):
				if matrix_module[j][1] != 'R' and matrix_module[j][1] != 'P' and matrix_module[j][1] != 'PR':
					log.errors = log.errors + "Type \"" + matrix_module[j][1] + "\" from module \"" + array_main[i] + "\" is invalid!\n"
					return 1
				
				if matrix_module[j][0] == None or matrix_module[j][1] == None or matrix_module[j][2] == None:
					log.errors = log.errors + "Invalid syntax for " + module.name + " module at the line " + str(j + MINUS_VAL + 1) + "!\n"
					return 1
				
				if check_name(matrix_module[j][0]) == 1:
					log.errors = log.errors + "Port name \"" + matrix_module[j][0] + "\" from module \"" + array_main[i] + "\" has invalid format!\n"
					return 1
				
				if check_name(matrix_module[j][2]) == 1:
					log.errors = log.errors + "Used interface \"" + matrix_module[j][2] + "\" from module \"" + array_main[i] + "\" has invalid format!\n"
					return 1
				
				if check_name(matrix_module[j][0]) == 1:
					log.errors = log.errors + "Invalid port name in module \"" + module.name + "\" at the line " + str(j + MINUS_VAL + 1) + ".(" + matrix_module[j][0] + ")\n"
				
				if check_name(matrix_module[j][2]) == 1:
					log.errors = log.errors + "Invalid used interface name in module \"" + module.name + "\" at the line " + str(j + MINUS_VAL + 1) + ".(" + matrix_module[j][2] + ")\n"
				
				if module.check_duplicate(matrix_module[j][2]) == 1:
					log.errors = log.errors + "Module:" + module.name + " port name \"" + matrix_module[j][2] + "\" is duplicate.\n"
				
				#duplicate
				len_interfaces_buffer = len(interfaces.buffer)
				found_interface = 0
				for k in range(0, len_interfaces_buffer):
					if interfaces.buffer[k].name == matrix_module[j][2]:
						found_interface = 1
						module.add_Member(matrix_module[j][0], matrix_module[j][1], interfaces.buffer[k])
						if matrix_module[j][1] == 'P':
							interfaces.buffer[k].used_p = 1
						if matrix_module[j][1] == 'R':
							interfaces.buffer[k].used_r = 1
				
				len_interfaces_iohwab = len(interfaces.iohwab)
				for k in range(0, len_interfaces_iohwab):
					if interfaces.iohwab[k].name == matrix_module[j][2]:
						found_interface = 1
						module.add_Member(matrix_module[j][0], matrix_module[j][1], interfaces.iohwab[k])
						if matrix_module[j][1] == 'P':
							interfaces.iohwab[k].used_p = 1
						if matrix_module[j][1] == 'R':
							interfaces.iohwab[k].used_r = 1
						
				#end_duplicate
				
				if found_interface == 0:
					log.errors = log.errors + "Interface \"" + matrix_module[j][2] + "\" from module \"" + module.name + "\" not found!\n"
					return 1
			
			for j in range(0, len_module_run):
				if array_module[j] == None:
					log.errors = log.errors + "Invalid syntax for " + module.name + " runnable at the line " + str(j + MINUS_VAL + 1) + "!\n"
					return 1
				
				if check_var(array_module[j]) == 1:
					log.errors = log.errors + "Invalid runnable name in module \"" + module.name + "\" at the line " + str(j + MINUS_VAL + 1) + ".(" + array_module[j] + ")\n"
					return 1
				
				runnable = Runnable(array_module[j])
				if module.check_RunnableDuplicate(array_module[j]) == 1:
					log.errors = log.errors + "Module:" + module.name + " runnable name \"" + array_module[j] + "\" is duplicate.\n"
					return 1
					
				module.add_Runnable(runnable)
			
			data.add_Module(module)
	
	return 0

def writer_struct(struct, name, initval):
	len_member_name = len(struct.member_name)
	for i in range(0, len_member_name):
		if struct.member_type[i].type == "Struct":
			writer_struct(struct.member_type[i], name + "." + struct.member_name[i], initval)
		else:
			recursive_func.log = recursive_func.log + name  + "." + struct.member_name[i] + " = " + initval.value[recursive_func.count] + ";\n"
			recursive_func.count = recursive_func.count + 1

def writer(interfaces, cluster):
	Rte_Buffers_h = open(dirname+"/../../../09_Templates/Empty/Code/Generic/Empty_Header.h", 'r');
	Rte_Buffers_c = open(dirname+"/../../../09_Templates/Empty/Code/Generic/Empty_Source.c", 'r')
	Rte_c = open(dirname+"/../../../09_Templates/Empty/Code/Generic/Empty_Source.c", 'r')
	lines_buffers_h = Rte_Buffers_h.readlines()
	lines_buffers_c = Rte_Buffers_c.readlines()
	lines_rte_c = Rte_c.readlines()
	Rte_Buffers_h.close()
	Rte_Buffers_c.close()
	Rte_c.close()
	
	Rte_Buffers_h = open(dirname+"/../../Code/Config/Rte_Buffers.h", 'w')
	Rte_Buffers_c = open(dirname+"/../../Code/Config/Rte_Buffers.c", 'w')
	Rte_c = open(dirname+"/../../Code/Config/Rte.c", 'w')
	
	lines_buffers_h[H_FILE_N_POS] = " *    \\file       Rte_Buffers.h\n"
	lines_buffers_h[H_AUTHOR_POS] = " *    \\author     " + AUTHOR_N + "\n"
	lines_buffers_h[H_BRIEF_POS] = " *    \\brief      " + BRIEF + "\n"
	lines_buffers_h[H_BRIEF_POS + 1] = BRIEF_BUFFERS_H + "\n"
	lines_buffers_h[H_IFNDEF_POS] = "#ifndef RTE_BUFFERS_H\n"
	lines_buffers_h[H_DEF_POS] = "#define RTE_BUFFERS_H\n"
	lines_buffers_h[H_INCLUSIONS_POS] = "\n#include \"Rte_Type.h\"\n" + "#include \"IoHwAb.h\"\n\n"
	lines_buffers_h[H_ENDIF_POS] = "#endif /* RTE_BUFFERS_H */"
	
	lines_buffers_c[C_FILE_N_POS] = " *    \\file       Rte_Buffers.c\n"
	lines_buffers_c[C_AUTHOR_POS] = " *    \\author     " + AUTHOR_N + "\n"
	lines_buffers_c[C_BRIEF_POS] = " *    \\brief      " + BRIEF + "\n"
	lines_buffers_c[C_BRIEF_POS + 1] = BRIEF_BUFFERS_C + "\n"
	lines_buffers_c[C_INCLUSIONS_POS] = "\n#include \"Rte_Buffers.h\"\n" + "#include \"Rte.h\"\n\n"
	
	lines_rte_c[C_FILE_N_POS] = " *    \\file       Rte.c\n"
	lines_rte_c[C_AUTHOR_POS] = " *    \\author     " + AUTHOR_N +"\n"
	lines_rte_c[C_BRIEF_POS] = " *    \\brief      " + BRIEF + "\n"
	lines_rte_c[C_BRIEF_POS + 1] = BRIEF_RTE_C + "\n"
	lines_rte_c[C_INCLUSIONS_POS] = "\n#include \"Rte.h\"\n\n"
	lines_rte_c[C_G_IMPL_FUNC_POS] = "/**\n * \\brief      Initializes all the RTE buffers and update flags with start values.\n * \\param      -\n * \\return     Always E_OK.\n */\nStd_ReturnType Rte_Start(void)\n{\n"
	
	lines_rte_c[C_G_IMPL_FUNC_POS] =  lines_rte_c[C_G_IMPL_FUNC_POS] + "   /* Buffers. */\n"
	lines_buffers_c[C_G_VAR_POS] =  lines_buffers_c[C_G_VAR_POS] + "/* Buffers. */\n"
	lines_buffers_h[H_G_VAR_POS] = 	lines_buffers_h[H_G_VAR_POS] + "/* Buffers. */\n"
	#BUFFER_WRITER
	len_buffer = len(interfaces.buffer)
	for i in range(0, len_buffer):
			len_buffer_var_data = len(interfaces.buffer[i].var_data)
			for j in range(0, len_buffer_var_data):
				if interfaces.buffer[i].dtype[j].type == "Struct":
					recursive_func.reset()
					writer_struct(interfaces.buffer[i].dtype[j], "   Rte_" + interfaces.buffer[i].name + "_" + interfaces.buffer[i].var_data[j] + "_Buf", interfaces.buffer[i].initval[j])
					lines_rte_c[C_G_IMPL_FUNC_POS] = lines_rte_c[C_G_IMPL_FUNC_POS] + recursive_func.log
				else:
					lines_rte_c[C_G_IMPL_FUNC_POS] = lines_rte_c[C_G_IMPL_FUNC_POS] + "   Rte_" + interfaces.buffer[i].name + "_" + interfaces.buffer[i].var_data[j] + "_Buf = " + interfaces.buffer[i].initval[j] + ";\n"
				
				result = interfaces.buffer[i].dtype[j].name + " Rte_" + interfaces.buffer[i].name + "_" + interfaces.buffer[i].var_data[j] + "_Buf;\n"
				lines_buffers_c[C_G_VAR_POS] = lines_buffers_c[C_G_VAR_POS] + result
				lines_buffers_h[H_G_VAR_POS] = lines_buffers_h[H_G_VAR_POS] + "extern " + result
			
			lines_buffers_c[C_G_VAR_POS] = lines_buffers_c[C_G_VAR_POS] + "\n"
			lines_buffers_h[H_G_VAR_POS] = lines_buffers_h[H_G_VAR_POS] + "\n"
			lines_rte_c[C_G_IMPL_FUNC_POS] = lines_rte_c[C_G_IMPL_FUNC_POS] + "\n"
	
	lines_rte_c[C_G_IMPL_FUNC_POS] = lines_rte_c[C_G_IMPL_FUNC_POS] + "   /* Update flags. */\n"
	lines_buffers_c[C_G_VAR_POS] =  lines_buffers_c[C_G_VAR_POS] + "/* Update flags. */\n"
	lines_buffers_h[H_G_VAR_POS] = lines_buffers_h[H_G_VAR_POS] + "/* Update flags. */\n"
	
	len_data = len(data.module)
	for index in range(0, len_data):
		Module = open(dirname+"/../../../09_Templates/Empty/Code/Generic/Empty_Header.h", 'r')
		lines_module = Module.readlines()
		Module.close()
		Module = open(dirname+"/../../Code/Config/Rte_" + data.module[index].name + ".h", 'w')

		lines_module[H_FILE_N_POS] = " *    \\file       Rte_" + data.module[index].name + ".h\n"
		lines_module[H_AUTHOR_POS] = " *    \\author     " + AUTHOR_N +"\n"
		lines_module[H_BRIEF_POS] = " *    \\brief      " + BRIEF + "\n"
		BRIEF_MODULE_H_COPY = BRIEF_MODULE_H
		BRIEF_MODULE_H_COPY = BRIEF_MODULE_H_COPY.replace("SwcExample", data.module[index].name)
		lines_module[H_BRIEF_POS + 1] = BRIEF_MODULE_H_COPY + "\n"
		lines_module[H_IFNDEF_POS] = "#ifndef RTE_" + data.module[index].name.upper() + "_H\n"
		lines_module[H_DEF_POS] = "#define RTE_" + data.module[index].name.upper() + "_H\n"
		lines_module[H_INCLUSIONS_POS] = "\n#include \"Rte.h\"\n\n"
		lines_module[H_ENDIF_POS] = "#endif /* RTE_" + data.module[index].name.upper() + "_H */"
		
		len_module = len(data.module[index].port_name)
		for count in range(0, len_module):
			#BUFFER
			if data.module[index].used_interface[count].type == "Buffer":
				len_var_data = len(data.module[index].used_interface[count].var_data)
				for i in range(0, len_var_data):			
					#BUFFER_R
					if data.module[index].port_type[count] == "R" or data.module[index].port_type[count] == "PR":
						#UPDATE_FLAG
						if data.module[index].used_interface[count].updateflag[i] == "x":
							#WRITE_BUFFER_FLAG
							result = "boolean Rte_" + data.module[index].used_interface[count].name + "_" + data.module[index].port_name[count] + "_"  + data.module[index].used_interface[count].var_data[i] + "_UpdateFlag;\n"
							lines_rte_c[C_G_IMPL_FUNC_POS] = lines_rte_c[C_G_IMPL_FUNC_POS] + "   Rte_" + data.module[index].used_interface[count].name + "_" + data.module[index].port_name[count] + "_"  + data.module[index].used_interface[count].var_data[i] + "_UpdateFlag = FALSE;\n"
							lines_buffers_c[C_G_VAR_POS] = lines_buffers_c[C_G_VAR_POS] + result
							lines_buffers_h[H_G_VAR_POS] = lines_buffers_h[H_G_VAR_POS] + "extern " + result
							
							#WRITE_RTE_MODULE
							lines_module[H_G_MACRO_POS] = lines_module[H_G_MACRO_POS] + "#define Rte_IsUpdated_" + data.module[index].port_name[count] + "_" + data.module[index].used_interface[count].var_data[i] + "() (Rte_" + data.module[index].used_interface[count].name + "_" + data.module[index].port_name[count] + "_" + data.module[index].used_interface[count].var_data[i] + "_UpdateFlag)\n"
							lines_module[H_G_MACRO_POS] = lines_module[H_G_MACRO_POS] + "#define Rte_Read_" + data.module[index].port_name[count] + "_" + data.module[index].used_interface[count].var_data[i] + "(x) \\\n   do \\\n   { \\\n      (*x) = Rte_" + data.module[index].used_interface[count].name + "_" + data.module[index].used_interface[count].var_data[i] + "_Buf; \\\n      Rte_" + data.module[index].used_interface[count].name + "_" + data.module[index].port_name[count] + "_" + data.module[index].used_interface[count].var_data[i] + "_UpdateFlag = FALSE; \\\n   } while(0)\n"
						#WITHOUT_UPDATE
						else:
							lines_module[H_G_MACRO_POS] = lines_module[H_G_MACRO_POS] + "#define Rte_Read_" + data.module[index].port_name[count] + "_" + data.module[index].used_interface[count].var_data[i] + "(x) ((*x) = Rte_" + data.module[index].used_interface[count].name + "_" + data.module[index].used_interface[count].var_data[i] + "_Buf)\n"
					#BUFFER_P
					if data.module[index].port_type[count] == "P" or data.module[index].port_type[count] == "PR":
						#UPDATE_FLAG
						if data.module[index].used_interface[count].updateflag[i] == "x":
							#WRITE_RTE_MODULE
							lines_module[H_G_MACRO_POS] = lines_module[H_G_MACRO_POS] + "#define Rte_Write_" + data.module[index].port_name[count] + "_" + data.module[index].used_interface[count].var_data[i] + "(x) \\\n   do \\\n   { \\\n      Rte_" + data.module[index].used_interface[count].name + "_" + data.module[index].used_interface[count].var_data[i] + "_Buf = (*x);"
							for j in range(0, len_data):
								len_module_current = len(data.module[j].port_name)
								for k in range(0, len_module_current):
									if data.module[j].used_interface[k].name == data.module[index].used_interface[count].name and data.module[index].port_name[count] != data.module[j].port_name[k]:
										lines_module[H_G_MACRO_POS] = lines_module[H_G_MACRO_POS] + " \\\n      Rte_" + data.module[index].used_interface[count].name + "_" + data.module[j].port_name[k] + "_" + data.module[index].used_interface[count].var_data[i] + "_UpdateFlag = TRUE;"
							lines_module[H_G_MACRO_POS] = lines_module[H_G_MACRO_POS] + " \\\n   } while(0)\n"
						#WITHOUT_UPDATE
						else:
							lines_module[H_G_MACRO_POS] = lines_module[H_G_MACRO_POS] + "#define Rte_Write_" + data.module[index].port_name[count] + "_" + data.module[index].used_interface[count].var_data[i] + "(x) (Rte_" + data.module[index].used_interface[count].name + "_" + data.module[index].used_interface[count].var_data[i] + "_Buf = (*x))\n"
			
			#IOHWAB
			elif data.module[index].used_interface[count].type == "IoHwAb":
				len_interfaces_type = len(data.module[index].used_interface[count].port_type)
				for i in range(0, len_interfaces_type):
					len_cluster = len(cluster)
					for j in range(0, len_cluster):
						len_component = len(cluster[j].component)
						for k in range(0,len_component):
							if data.module[index].used_interface[count].port_type[i] == cluster[j].component[k].cluster_name:
								len_directions = len(cluster[j].component[k].directions)
								for l in range(0, len_directions):
									if data.module[index].port_type[count] == cluster[j].component[k].directions[l]:
										if data.module[index].port_type[count] == "PR":
											log.errors = log.errors + "PR not allowed for IoHwAb\n"
										else:
											#IOHWAB_R
											if data.module[index].port_type[count] == "R":
												lines_module[H_G_MACRO_POS] = lines_module[H_G_MACRO_POS] + "#define Rte_Read_" + data.module[index].port_name[count] + "_" + data.module[index].used_interface[count].var_data[i] + "(x) ((*x) = " + cluster[j].component[k].functions[l] + "(" + data.module[index].used_interface[count].channel_name[i] + "))\n"
											#IOHWAB_P
											if data.module[index].port_type[count] == "P":
												lines_module[H_G_MACRO_POS] = lines_module[H_G_MACRO_POS] + "#define Rte_Write_" + data.module[index].port_name[count] + "_" + data.module[index].used_interface[count].var_data[i] + "(x) (" + cluster[j].component[k].functions[l] + "(" + data.module[index].used_interface[count].channel_name[i] + ", (*x)))\n"
		
			lines_module[H_G_MACRO_POS] = lines_module[H_G_MACRO_POS] + "\n"
			
		Module.writelines(lines_module)
		Module.close()

	lines_buffers_c[C_G_VAR_POS] = lines_buffers_c[C_G_VAR_POS] + "\n"
	lines_buffers_h[H_G_VAR_POS] = lines_buffers_h[H_G_VAR_POS] + "\n"
	lines_rte_c[C_G_IMPL_FUNC_POS] = lines_rte_c[C_G_IMPL_FUNC_POS] + "\n   return E_OK;\n}\n"
	Rte_Buffers_h.writelines(lines_buffers_h)
	Rte_Buffers_h.close()
	Rte_Buffers_c.writelines(lines_buffers_c)
	Rte_Buffers_c.close()
	Rte_c.writelines(lines_rte_c)
	Rte_c.close()
	
	return 0

def check():
	len_module = len(data.module)
	for i in range(0, len_module):
		len_runnable = len(data.module[i].runnable)
		for j in range(0, len_runnable):
			if data.module[i].runnable[j].used == 0:
				log.warnings = log.warnings + "Runnable \"" + data.module[i].runnable[j].name + "\" is not used." + "\n"
	
	return 0