/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       IoHwAb_ChannelsCfg.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Defines the sensor and actuator channels on the ECU abstraction level for accessing all the ECU
 *                signals.
 *
 *    From the MCAL drivers' perspective the channels do not contain the information about the context they're used in.
 *    In this layer the MCAL channels are mapped to ECU signal names that describe the signals origin or destination
 *    that are correlated with a sensor or an actuator.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef IOHWAB_CHANNELSCFG_H
#define IOHWAB_CHANNELSCFG_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "IoHwAb_ApiCfg.h"
#include "Dio.h"
#include "Pwm.h"

#if (STD_ON == IOHWAB_API)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/* Redefine all the MCAL driver channels by providing the information about the used peripherals. */

/* -------------------------- Analog ---------------------------*/

#if (STD_ON == IOHWAB_ANALOG_API)

#define IOHWAB_ANALOG_POTENTIOMETER                (ADC_C_5)

#endif /* (STD_ON == IOHWAB_ANALOG_API) */

/* -------------------------- Digital --------------------------*/

#if (STD_ON == IOHWAB_DIGITAL_API)

#define IOHWAB_DIGITAL_LED_GREEN                   (DIO_E_5)
#define IOHWAB_DIGITAL_LED_YELLOW                  (DIO_E_1)
#define IOHWAB_DIGITAL_LED_RED                     (DIO_E_3)
#define IOHWAB_DIGITAL_LED_GREEN_ENGINE            (DIO_E_4)
#define IOHWAB_DIGITAL_LED_YELLOW_ENGINE           (DIO_E_0)
#define IOHWAB_DIGITAL_LED_RED_ENGINE              (DIO_E_2)

#define IOHWAB_DIGITAL_L_SIDE_MIRROR_LEFT          (DIO_D_0)
#define IOHWAB_DIGITAL_L_SIDE_MIRROR_RIGHT         (DIO_D_3)
#define IOHWAB_DIGITAL_R_SIDE_MIRROR_LEFT          (DIO_D_10)
#define IOHWAB_DIGITAL_R_SIDE_MIRROR_RIGHT         (DIO_D_1)
#define IOHWAB_DIGITAL_STEERING_WHEEL_UP           (DIO_D_8)
#define IOHWAB_DIGITAL_STEERING_WHEEL_DOWN         (DIO_D_4)

#define IOHWAB_DIGITAL_ENGINE_ON                   (DIO_D_2)
#define IOHWAB_DIGITAL_ENGINE_OFF                  (DIO_D_9)

#define IOHWAB_DIGITAL_RFID_TX                     (DIO_B_10)
#define IOHWAB_DIGITAL_RFID_RX                     (DIO_B_11)

#endif /* (STD_ON == IOHWAB_DIGITAL_API) */

/* ---------------------------- PWM ----------------------------*/

#if (STD_ON == IOHWAB_PWM_API)

#define IOHWAB_PWM_SERVO_LEFT_SIDE_MIRROR          (PWM_B_1)
#define IOHWAB_PWM_SERVO_RIGHT_SIDE_MIRROR         (PWM_B_5)
#define IOHWAB_PWM_SERVO_STEERING_WHEEL            (PWM_B_6)

#endif /* (STD_ON == IOHWAB_PWM_API) */

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#endif /* (STD_ON == IOHWAB_API) */

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* IOHWAB_CHANNELSCFG_H */
