/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pwm.h
 *    \author     Alexandra Purcaru
 *    \brief      Exports the data types and the interfaces used for implementations.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef PWM_H
#define PWM_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "Pwm_Cfg.h"
#include "stm32f407xx.h"
#include "RegInit.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defining the channel configuration table containing the duty cycle and the period of the timer. */
typedef struct
{
   /** \brief  Defines the duty cycle of the channel. */
   volatile uint32* pul_DutyCycleReg;
   /** \brief  Defines the specific period of the channel. */
   volatile uint32* pul_PeriodReg;
} Pwm_ChannelConfigType;

/** \brief  Defining the configuration table.*/
typedef struct
{
   /** \brief  Defines the pointer to the initialization registers. */
   const RegInit_Masked32BitsConfigType* kpt_Registers;
   /** \brief  Defines the pointer to the channel configuration registers. */
   const Pwm_ChannelConfigType* kpt_Channels;
} Pwm_ConfigType;

/** \brief  Defines the numeric identifier of a PWM channel. */
typedef uint8 Pwm_ChannelType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const Pwm_ConfigType Pwm_gkt_Config;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void Pwm_Init(const Pwm_ConfigType* ConfigPtr);
extern void Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber, uint16 DutyCycle);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* PWM_H */
