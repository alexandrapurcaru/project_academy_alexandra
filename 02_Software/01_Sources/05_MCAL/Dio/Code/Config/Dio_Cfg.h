/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Dio_Cfg.h
 *    \author     Alexandra Purcaru
 *    \brief      Defines the channel IDs and the port IDs.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef DIO_CFG_H
#define DIO_CFG_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the channel ID for port D, pin 0. */
#define DIO_D_0   (0U)
/** \brief  Defines the channel ID for port D, pin 1. */
#define DIO_D_1   (1U)
/** \brief  Defines the channel ID for port D, pin 2. */
#define DIO_D_2   (2U)
/** \brief  Defines the channel ID for port D, pin 3. */
#define DIO_D_3   (3U)
/** \brief  Defines the channel ID for port D, pin 4. */
#define DIO_D_4   (4U)
/** \brief  Defines the channel ID for port D, pin 8. */
#define DIO_D_8   (5U)
/** \brief  Defines the channel ID for port D, pin 9. */
#define DIO_D_9   (6U)
/** \brief  Defines the channel ID for port D, pin 10. */
#define DIO_D_10  (7U)
/** \brief  Defines the channel ID for port D, pin 11. */
#define DIO_D_11  (8U)
/** \brief  Defines the channel ID for port D, pin 12. */
#define DIO_D_12  (9U)
/** \brief  Defines the channel ID for port E, pin 0. */
#define DIO_E_0   (10U)
/** \brief  Defines the channel ID for port E, pin 1. */
#define DIO_E_1   (11U)
/** \brief  Defines the channel ID for port E, pin 2. */
#define DIO_E_2   (12U)
/** \brief  Defines the channel ID for port E, pin 3. */
#define DIO_E_3   (13U)
/** \brief  Defines the channel ID for port E, pin 4. */
#define DIO_E_4   (14U)
/** \brief  Defines the channel ID for port E, pin 5. */
#define DIO_E_5   (15U)
/** \brief  Defines the channel ID for port E, pin 6. */
#define DIO_E_6   (16U)
/** \brief  Defines the channel ID for port E, pin 7. */
#define DIO_E_7   (17U)
/** \brief  Defines total number of channels. */
#define DIO_NR_OF_CHANNELS (18U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* DIO_CFG_H */
