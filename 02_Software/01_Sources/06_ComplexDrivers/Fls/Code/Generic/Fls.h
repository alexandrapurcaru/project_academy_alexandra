/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Fls.h
 *    \author     Alexandra Purcaru
 *    \brief      Exports the data types and the interfaces used for implementations.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef FLS_H
#define FLS_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief   Defines the base address configuration table. */
typedef struct
{
   /** \brief  Defines the base address of the FLASH memory. */
   FLASH_TypeDef* pt_BaseAddress;
} Fls_ChannelConfigType;

typedef enum
{
FLS_IDLE,
FLS_BUSY,
}MemIf_StatusType;

/** \brief  Used as address offset from the configured flash base address to access a certain flash memory area. */
typedef uint32 Fls_AddressType;

/** \brief  Specifies the number of bytes to read/write/erase.. */
typedef uint8 Fls_LengthType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern MemIf_StatusType Fls_gv_GetStatus(void);
extern Std_ReturnType Fls_gv_Erase(Fls_AddressType TargetAddress, Fls_LengthType Length);
extern Std_ReturnType Fls_gv_Write(Fls_AddressType TargetAddress, const uint8* SourceAddressPtr, Fls_LengthType Length);
extern Std_ReturnType Fls_gv_Read(Fls_AddressType SourceAddress,uint8* TargetAddressPtr,Fls_LengthType Length);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* FLS_H */
