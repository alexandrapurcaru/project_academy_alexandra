/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Dio_Lcfg.c
 *    \author     Alexandra Purcaru
 *    \brief      Defines the configuration for DIO channels and ports.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Dio.h"
#include "Dio_Cfg.h"
#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the registers table that creates a link between the base address and the necessary masks. */
const Dio_ChannelConfigType Dio_gkat_ChannelConfig[DIO_NR_OF_CHANNELS] =
   {

      /*    Sets pin 0 as an input, using the base address for port D and the necessary mask.  */
      { GPIOD, (uint16) GPIO_IDR_IDR_0 },
      /*    Sets pin 1 as an input, using the base address for port D and the necessary mask.  */
      { GPIOD, (uint16) GPIO_IDR_IDR_1 },
      /*    Sets pin 2 as an input, using the base address for port D and the necessary mask.  */
      { GPIOD, (uint16) GPIO_IDR_IDR_2 },
      /*    Sets pin 3 as an input, using the base address for port D and the necessary mask.  */
      { GPIOD, (uint16) GPIO_IDR_IDR_3 },
      /*    Sets pin 4 as an input, using the base address for port D and the necessary mask.  */
      { GPIOD, (uint16) GPIO_IDR_IDR_4 },
      /*    Sets pin 8 as an input, using the base address for port D and the necessary mask.  */
      { GPIOD, (uint16) GPIO_IDR_IDR_8 },
      /*    Sets pin 9 as an input, using the base address for port D and the necessary mask.  */
      { GPIOD, (uint16) GPIO_IDR_IDR_9 },
      /*    Sets pin 10 as an input, using the base address for port D and the necessary mask.  */
      { GPIOD, (uint16) GPIO_IDR_IDR_10 },
      /*    Sets pin 0 as an output, using the base address for port E and the necessary mask.  */
      { GPIOE, (uint16) GPIO_ODR_ODR_0 },
      /*    Sets pin 1 as an output, using the base address for port E and the necessary mask.  */
      { GPIOE, (uint16) GPIO_ODR_ODR_1 },
      /*    Sets pin 2 as an output, using the base address for port E and the necessary mask.  */
      { GPIOE, (uint16) GPIO_ODR_ODR_2 },
      /*    Sets pin 3 as an output, using the base address for port E and the necessary mask.  */
      { GPIOE, (uint16) GPIO_ODR_ODR_3 },
      /*    Sets pin 4 as an output, using the base address for port E and the necessary mask.  */
      { GPIOE, (uint16) GPIO_ODR_ODR_4 },
      /*    Sets pin 5 as an output, using the base address for port E and the necessary mask.  */
      { GPIOE, (uint16) GPIO_ODR_ODR_5 },
      /*    Sets pin 6 as an output, using the base address for port E and the necessary mask.  */
      { GPIOE, (uint16) GPIO_IDR_IDR_6 },
      /*    Sets pin 7 as an output, using the base address for port E and the necessary mask.  */
      { GPIOE, (uint16) GPIO_IDR_IDR_7 },
   };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
