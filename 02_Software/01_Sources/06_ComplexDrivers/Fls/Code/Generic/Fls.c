/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Fls.c
 *    \author     Alexandra Purcaru
 *    \brief      Implements the FLS interfaces.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Fls.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the key 1 value for the key register. */
#define FLS_KEYR_KEY1                  (0x45670123)
/** \brief  Defines the key 2 value for the key register. */
#define FLS_KEYR_KEY2                  (0xCDEF89AB)

#define FLS_NUMBER_OF_SECTORS          (7U)

/** \brief  Defines the key 1 value for the key register. */
#define FEE_KEYR_KEY1                  (0x45670123)
/** \brief  Defines the key 2 value for the key register. */
#define FEE_KEYR_KEY2                  (0xCDEF89AB)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      The function erases flash sectors.
 * \param      TargetAddress: Target address in flash memory.
 * \param      Length: Number of bytes to erase.
 * \return     E_OK: erase command has been accepted, E_NOT_OK: erase command has not been accepted
 */
Std_ReturnType Fls_gv_Erase(Fls_AddressType TargetAddress, Fls_LengthType Length)
{
   Length = Length + 0;

   FLASH->CR |= FLASH_CR_STRT;

   TargetAddress = TargetAddress + 0;

//      *(Fls_AddressType*) TargetAddress = *(Fls_AddressType*) 0xFFFFFFFF;

   return E_OK;
}

/**
 * \brief      The function writes one or more complete flash pages.
 * \param      TargetAddress: Target address in flash memory.
 * \param      SourceAddressPtr: Pointer to source data buffer.
 * \param      Length: Number of bites to be written.
 * \return     E_OK: write command has been accepted, E_NOT_OK: write command has not been accepted
 */
Std_ReturnType Fls_gv_Write(Fls_AddressType TargetAddress, const uint8* SourceAddressPtr, Fls_LengthType Length)
{
   Length = Length + 0;

//   FLASH->CR |= FLASH_CR_PG;

      /*    Writes the desired data in the specified address in flash memory. */
      *(volatile Fls_AddressType*) TargetAddress = *(Fls_AddressType*)SourceAddressPtr;
//         TargetAddress++;
//         SourceAddressPtr++;
//      Length--;
//   FLASH->CR &= ~FLASH_CR_PG;

   return E_OK;
}

/**
 * \brief      The function reads data from flash memory.
 * \param      SourceAddress: Source address in flash memory.
 * \param      TargetAddressPtr: Pointer to target data buffer.
 * \param      Length: Number of bytes to read.
 * \return     E_OK: read command has been accepted, E_NOT_OK: read command has not been accepted.
 */
Std_ReturnType Fls_gv_Read(Fls_AddressType SourceAddress, uint8* TargetAddressPtr, Fls_LengthType Length)
{
//   FLASH->ACR |= FLASH_ACR_LATENCY_2WS;

   while ((Length) != 0x00)
   {
      /*    Reads the data from the desired address in the flash memory. */
      *(Fls_AddressType*)TargetAddressPtr = *(volatile Fls_AddressType*) SourceAddress;
      SourceAddress++;
      TargetAddressPtr++;
      Length--;
   }

   return E_OK;
}
