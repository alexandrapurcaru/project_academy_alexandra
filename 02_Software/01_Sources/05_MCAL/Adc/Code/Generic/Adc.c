/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Adc.c
 *    \author     Alexandra Purcaru
 *    \brief      Implements the ADC interfaces.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Adc.h"
#include "RegInit.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defining the result buffer. */
static Adc_ValueGroupType* Adc_apt_Result[ADC_NR_OF_CHANNELS];

/** \brief  Defining the result buffer. */
static Adc_ChannelConfigType* Adc_pt_Channels;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      The function initializes the ADC hardware units and driver.
 * \param      ConfigPtr: Pointer to configuration set.
 * \return     -
 */
void Adc_Init(const Adc_ConfigType* ConfigPtr)
{
   Adc_pt_Channels = (Adc_ChannelConfigType*) ConfigPtr->kpt_Channels;

   RegInit_gv_Masked32Bits(ConfigPtr->kpt_Registers);
}
/**
 * \brief      Initializes ADC driver with the group specific result buffer start address where the conversion results
 *             will be stored.
 * \param      Group: Numeric ID of requested ADC channel group.
 * \param      DataBufferPtr: Pointer to result data buffer.
 * \return     The result of the initialization: E_OK if the result buffer pointer initialized correctly, E_NOT_OK if
 *             the operation failed or development error occurred.
 */
Std_ReturnType Adc_SetupResultBuffer(Adc_GroupType Group, const Adc_ValueGroupType* DataBufferPtr)
{
   Adc_apt_Result[Group] = (Adc_ValueGroupType*) DataBufferPtr;

   return E_OK;
}

/**
 * \brief      The function starts the conversion of all channels of the requested ADC Channel group.
 * \param      Group: Numeric ID of requested ADC Channel group.
 * \return     -
 */
void Adc_StartGroupConversion(Adc_GroupType Group)
{
   /*  Assigns a sequence for the group to be converted. */
   Adc_pt_Channels[Group].pt_BaseAddress->SQR3 |= Adc_pt_Channels[Group].ul_ChannelNumber;
   /*  Starts conversion of regular channels. */
   Adc_pt_Channels[Group].pt_BaseAddress->CR2 |= ADC_CR2_SWSTART;
}

/**
 * \brief      The function verifies the conversion status of the requested ADC Channel group.
 * \param      Group: Numeric ID of requested ADC Channel group.
 * \return     Conversion status for the requested group.
 */
Adc_StatusType Adc_GetGroupStatus(Adc_GroupType Group)
{
   Adc_StatusType t_Status = ADC_IDLE;

   /*    The conversion has been started. */
   if ((Adc_pt_Channels[Group].pt_BaseAddress->SR & ADC_SR_STRT) != 0x00)
   {
      /*    The conversion has been finished. */
      if ((Adc_pt_Channels[Group].pt_BaseAddress->SR & ADC_SR_EOC) != 0x00)
      {
         t_Status = ADC_COMPLETED;
         /*    The result buffer takes the result value from the conversion. */
         *Adc_apt_Result[Group] = Adc_pt_Channels[Group].pt_BaseAddress->DR;
         /*    The start register is being set to zero, so a new conversion can start. */
         Adc_pt_Channels[Group].pt_BaseAddress->SR &= ~(ADC_SR_STRT);
      }
      else
      {
         /*    The conversion has been started and is still going on. */
         t_Status = ADC_BUSY;
      }
   }
   else
   {
      t_Status = ADC_IDLE;
   }

   return t_Status;
}
