/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Adc_Pcfg.c
 *    \author     Alexandra Purcaru
 *    \brief      Defines the control registers list for setting the HW ADC unit in single shot mode and sets the
 *                channels to be used.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Adc.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the total number of registers for ADC1, ADC2 and ADC3(5 registers for each converter). */
#define ADC_LOCAL_NR_OF_REGISTERS  (10U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the registers array. */
static const RegInit_Masked32BitsSingleType Adc_kat_ChannelsConfig[ADC_LOCAL_NR_OF_REGISTERS] =
   {
      /*    Enable ADC1 conversion. */
      { & ADC1->CR2, (uint32) ADC_CR2_ADON, ADC_CR2_ADON },
      /*    Sets the 12-bit resolution for the conversion at 15 ADCCLK cycles. */
      { & ADC1->CR1, (uint32) ~ADC_CR1_RES, 0x00 },
      /*    Sets the data alignment to right. */
      { & ADC1->CR2, (uint32) 0xFF, ADC_CR2_ALIGN },
      /*    Sets the sampling time for pin 0 at 144 cycles. */
      { & ADC1->SMPR2, (uint32) ~ADC_SMPR2_SMP0_0, ADC_SMPR2_SMP0_0 },
      /*    Sets the total number of conversions to 1. */
      { & ADC1->SQR1, (uint32) ~ADC_SQR1_L, ~ADC_SQR1_L },

      /*    Enable ADC2 conversion. */
      { & ADC2->CR2, (uint32) ADC_CR2_ADON, ADC_CR2_ADON },
      /*    Sets the 12-bit resolution for the conversion at 15 ADCCLK cycles. */
      { & ADC2->CR1, (uint32) ~ADC_CR1_RES, 0x00 },
      /*    Sets the data alignment to right. */
      { & ADC2->CR2, (uint32) 0xFF, ADC_CR2_ALIGN },
      /*    Sets the sampling time for pin 0 at 144 cycles. */
      { & ADC2->SMPR2, (uint32) ~ADC_SMPR2_SMP0_0, ADC_SMPR2_SMP0_0 },
      /*    Sets the total number of conversions to 1. */
      { & ADC2->SQR1, (uint32) ~ADC_SQR1_L, ~ADC_SQR1_L },

   };

/** \brief  Defines the registers table that creates a link between the base address and the necessary value of
 *          the channels. */
static const Adc_ChannelConfigType Adc_kat_Channels[ADC_NR_OF_CHANNELS] =
   {
      /*    Assigns pin 3 of port A as the 1st in the sequence to be converted. */
      { ADC1, (uint32) ADC_SQR3_SQ1_0 | ADC_SQR3_SQ1_1 },
      /*    Assigns pin 4 of port A as the 1nd in the sequence to be converted. */
      { ADC2, (uint32) ADC_SQR3_SQ1_2 },
   };

/** \brief  Defines the array containing the configuration registers and the number of registers. */
static const RegInit_Masked32BitsConfigType Adc_kt_Config =
   { Adc_kat_ChannelsConfig, ADC_LOCAL_NR_OF_REGISTERS };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the structure containing the configuration registers and the channels to be used. */
const Adc_ConfigType Adc_gkt_Config =
   { &Adc_kt_Config, Adc_kat_Channels };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

