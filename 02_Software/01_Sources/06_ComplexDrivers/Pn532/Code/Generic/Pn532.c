/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pn532.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Implements the PN532 UART state machine for reading the target IDs.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "Pn532_Cfg.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#if (((PN532_TIMEOUT_US / PN532_MAIN_PERIODICITY_US) * PN532_MAIN_PERIODICITY_US) \
      == PN532_TIMEOUT_US)

#define PN532_TIMEOUT_COUNT (PN532_TIMEOUT_US / PN532_MAIN_PERIODICITY_US)

#else

#error "Invalid configuration. PN532_TIMEOUT_US shall be a multiple of PN532_MAIN_PERIODICITY_US."

#endif

#if (((PN532_SCAN_TARGET_PERIODICITY_US / PN532_MAIN_PERIODICITY_US) * PN532_MAIN_PERIODICITY_US) \
      == PN532_SCAN_TARGET_PERIODICITY_US)

#define PN532_SCAN_TARGET_PERIODICITY_COUNT (PN532_READ_TARGET_PERIODICITY_US / PN532_MAIN_PERIODICITY_US)

#else

#error "Invalid configuration. PN532_TIMEOUT_US shall be a multiple of PN532_MAIN_PERIODICITY_US."

#endif

#define PN532_RECEPTION_BUFFER_LENGTH                 (19U)

#define PN532_WAKEUP_PATTERN_LENGTH                   (6U)
#define PN532_ACK_FRAME_LENGTH                        (6U)
#define PN532_TX_RF_CONFIG_FRAME_LENGTH               (13U)
#define PN532_RX_RF_CONFIG_FRAME_LENGTH               (9U)
#define PN532_TX_SAM_CONFIG_FRAME_LENGTH              (12U)
#define PN532_RX_SAM_CONFIG_FRAME_LENGTH              (9U)
#define PN532_TX_IN_LIST_PASSIVE_TARGET_FRAME_LENGTH  (11U)
#define PN532_RX_IN_LIST_PASSIVE_TARGET_FRAME_LENGTH  (19U)

#define PN532_RX_IN_LIST_PASSIVE_TARGET_ID_OFFSET     (13U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

typedef enum
{
   PN532_STATE_WAKEUP,
   PN532_STATE_RF_CONFIG,
   PN532_STATE_SAM_CONFIG,
   PN532_STATE_REQUEST_TARGET,
   PN532_STATE_READ_TARGET,
   PN532_STATE_IDLE,
} Pn532_StateType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

static uint8 Pn532_auc_ReceptionBuffer[PN532_RECEPTION_BUFFER_LENGTH];
static uint8 Pn532_uc_TxIndex;
static uint8 Pn532_uc_RxIndex;
static uint16 Pn532_us_TimeoutCounter;
static uint16 Pn532_us_ScanTargetCounter;
static Pn532_StateType Pn532_t_State;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

static const uint8 Pn532_kauc_RxAckPositiveFrame[PN532_ACK_FRAME_LENGTH] =
   { 0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00 };

static const uint8 Pn532_kauc_TxWakeupFrame[PN532_WAKEUP_PATTERN_LENGTH] =
   { 0x55, 0x55, 0x00, 0x00, 0x00, 0x00 };

static const uint8 Pn532_kauc_TxRfConfigFrame[PN532_TX_RF_CONFIG_FRAME_LENGTH] =
   { 0x00, 0x00, 0xFF, 0x06, 0xFA, 0xD4, 0x32, 0x05, 0xFF, 0x01, 0xFF, 0xF6, 0x00 };

static const uint8 Pn532_kauc_RxRfConfigFrame[PN532_RX_RF_CONFIG_FRAME_LENGTH] =
   { 0x00, 0x00, 0xFF, 0x02, 0xFE, 0xD5, 0x33, 0xF8, 0x00 };

static const uint8 Pn532_kauc_TxSamConfigFrame[PN532_TX_SAM_CONFIG_FRAME_LENGTH] =
   { 0x00, 0x00, 0xFF, 0x05, 0xFB, 0xD4, 0x14, 0x01, 0x14, 0x01, 0x02, 0x00 };

static const uint8 Pn532_kauc_RxSamConfigFrame[PN532_RX_SAM_CONFIG_FRAME_LENGTH] =
   { 0x00, 0x00, 0xFF, 0x02, 0xFE, 0xD5, 0x15, 0x16, 0x00 };

static const uint8 Pn532_kauc_TxInListPassiveTargetFrame[PN532_TX_IN_LIST_PASSIVE_TARGET_FRAME_LENGTH] =
   { 0x00, 0x00, 0xFF, 0x04, 0xFC, 0xD4, 0x4A, 0x01, 0x00, 0xE1, 0x00 };

static const uint8 Pn532_kauc_RxInListPassiveTargetFrame[PN532_RX_IN_LIST_PASSIVE_TARGET_ID_OFFSET] =
   { 0x00, 0x00, 0xFF, 0x0C, 0xF4, 0xD5, 0x4B, 0x01, 0x01, 0x00, 0x04, 0x08, 0x04 };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief     Sets up the PN532 hardware units and loads the channel configurations for runtime use.
 * \param     -
 * \return    -
 */
void Pn532_gv_Init(void)
{
   uint8 uc_I;
   for (uc_I = 0U; uc_I < PN532_RECEPTION_BUFFER_LENGTH; uc_I++)
   {
      Pn532_auc_ReceptionBuffer[uc_I] = 0xAAU;
   }

   Pn532_uc_TxIndex = 0U;
   Pn532_uc_RxIndex = 0U;
   Pn532_us_TimeoutCounter = 0U;
   Pn532_us_ScanTargetCounter = 0U;
   Pn532_t_State = PN532_STATE_WAKEUP;
}

/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Implements the PN532 state machine for reading the target IDs.
 * \param      -
 * \return     -
 */
void Pn532_gv_Main(void)
{
   uint8 uc_I;
   uint32 ul_TargetID;

   switch (Pn532_t_State)
   {
      case PN532_STATE_WAKEUP:
      {
         if (UART_TX_READY == Uart_gt_GetTransmitStatus(PN532_UART_TX))
         {
            Uart_gv_Transmit( PN532_UART_TX, (uint8*) &Pn532_kauc_TxWakeupFrame[Pn532_uc_TxIndex]);
            Pn532_uc_TxIndex++;

            if (Pn532_uc_TxIndex >= PN532_WAKEUP_PATTERN_LENGTH)
            {
               Pn532_uc_TxIndex = 0U;
               Pn532_t_State = PN532_STATE_RF_CONFIG;
            }
            else
            {
               /* Wakeup frame has not been fully sent yet. */
            }
         }
         else
         {
            /* Nothing to do. */
         }
         break;
      }

      /*-------------------------------------------------------------------------------------------------------------*/
      case PN532_STATE_RF_CONFIG:
      {
         if (Pn532_uc_TxIndex < PN532_TX_RF_CONFIG_FRAME_LENGTH)
         {
            if (UART_TX_READY == Uart_gt_GetTransmitStatus(PN532_UART_TX))
            {
               Uart_gv_Transmit(PN532_UART_TX, (uint8*) &Pn532_kauc_TxRfConfigFrame[Pn532_uc_TxIndex]);
               Pn532_uc_TxIndex++;

               if (Pn532_uc_TxIndex >= PN532_TX_RF_CONFIG_FRAME_LENGTH)
               {
                  Pn532_uc_RxIndex = 0U;
                  Pn532_us_TimeoutCounter = 0U;
               }
               else
               {
                  /* RF config frame has not been fully sent yet. */
               }
            }
            else
            {
               /* Nothing to do. */
            }
         }
         else
         {
            if (UART_RX_READY == Uart_gt_GetReceiveStatus(PN532_UART_RX))
            {
               Uart_gv_Receive(PN532_UART_RX, &Pn532_auc_ReceptionBuffer[Pn532_uc_RxIndex]);

               Pn532_us_TimeoutCounter = 0U;
               Pn532_uc_RxIndex++;

               if (Pn532_uc_RxIndex >= (PN532_ACK_FRAME_LENGTH + PN532_RX_RF_CONFIG_FRAME_LENGTH))
               {
                  Pn532_uc_TxIndex = 0U;
                  Pn532_t_State = PN532_STATE_SAM_CONFIG;

                  for (uc_I = 0U; uc_I < PN532_ACK_FRAME_LENGTH; uc_I++)
                  {
                     if (Pn532_auc_ReceptionBuffer[uc_I] != Pn532_kauc_RxAckPositiveFrame[uc_I])
                     {
                        Pn532_gv_Init();
                        uc_I = PN532_ACK_FRAME_LENGTH;
                     }
                     else
                     {
                     }
                  }

                  for (uc_I = PN532_ACK_FRAME_LENGTH;
                  uc_I < (PN532_ACK_FRAME_LENGTH + PN532_RX_RF_CONFIG_FRAME_LENGTH); uc_I++)
                  {
                     if (Pn532_auc_ReceptionBuffer[uc_I]
                        != Pn532_kauc_RxRfConfigFrame[uc_I - PN532_ACK_FRAME_LENGTH])
                     {
                        Pn532_gv_Init();
                        uc_I = PN532_ACK_FRAME_LENGTH;
                     }
                     else
                     {
                     }
                  }
               }
               else
               {
               }
            }
            else
            {
               Pn532_us_TimeoutCounter++;
               if (Pn532_us_TimeoutCounter >= PN532_TIMEOUT_COUNT)
               {
                  Pn532_gv_Init();
               }
            }
         }
         break;
      }

      /*-------------------------------------------------------------------------------------------------------------*/
      case PN532_STATE_SAM_CONFIG:
      {
         if (Pn532_uc_TxIndex < PN532_TX_SAM_CONFIG_FRAME_LENGTH)
         {
            if (UART_TX_READY == Uart_gt_GetTransmitStatus(PN532_UART_TX))
            {
               Uart_gv_Transmit(PN532_UART_TX, (uint8*) &Pn532_kauc_TxSamConfigFrame[Pn532_uc_TxIndex]);
               Pn532_uc_TxIndex++;

               if (Pn532_uc_TxIndex >= PN532_TX_SAM_CONFIG_FRAME_LENGTH)
               {
                  Pn532_uc_RxIndex = 0U;
                  Pn532_us_TimeoutCounter = 0U;
               }
               else
               {
                  /* RF config frame has not been fully sent yet. */
               }
            }
            else
            {
               /* Nothing to do. */
            }
         }
         else
         {
            if (UART_RX_READY == Uart_gt_GetReceiveStatus(PN532_UART_RX))
            {
               Uart_gv_Receive(PN532_UART_RX, &Pn532_auc_ReceptionBuffer[Pn532_uc_RxIndex]);

               Pn532_us_TimeoutCounter = 0U;
               Pn532_uc_RxIndex++;

               if (Pn532_uc_RxIndex >= (PN532_ACK_FRAME_LENGTH + PN532_RX_SAM_CONFIG_FRAME_LENGTH))
               {
                  Pn532_uc_TxIndex = 0U;
                  Pn532_t_State = PN532_STATE_REQUEST_TARGET;

                  for (uc_I = 0U; uc_I < PN532_ACK_FRAME_LENGTH; uc_I++)
                  {
                     if (Pn532_auc_ReceptionBuffer[uc_I] != Pn532_kauc_RxAckPositiveFrame[uc_I])
                     {
                        Pn532_gv_Init();
                        uc_I = PN532_ACK_FRAME_LENGTH;
                     }
                     else
                     {
                     }
                  }

                  for (uc_I = PN532_ACK_FRAME_LENGTH;
                  uc_I < (PN532_ACK_FRAME_LENGTH + PN532_RX_SAM_CONFIG_FRAME_LENGTH); uc_I++)
                  {
                     if (Pn532_auc_ReceptionBuffer[uc_I]
                        != Pn532_kauc_RxSamConfigFrame[uc_I - PN532_ACK_FRAME_LENGTH])
                     {
                        Pn532_gv_Init();
                        uc_I = PN532_ACK_FRAME_LENGTH;
                     }
                     else
                     {
                     }
                  }
               }
               else
               {
               }
            }
            else
            {
               Pn532_us_TimeoutCounter++;
               if (Pn532_us_TimeoutCounter >= PN532_TIMEOUT_COUNT)
               {
                  Pn532_gv_Init();
               }
            }
         }
         break;
      }

      /*-------------------------------------------------------------------------------------------------------------*/
      case PN532_STATE_REQUEST_TARGET:
      {
         if (Pn532_uc_TxIndex < PN532_TX_IN_LIST_PASSIVE_TARGET_FRAME_LENGTH)
         {
            if (UART_TX_READY == Uart_gt_GetTransmitStatus(PN532_UART_TX))
            {
               Uart_gv_Transmit(PN532_UART_TX, (uint8*) &Pn532_kauc_TxInListPassiveTargetFrame[Pn532_uc_TxIndex]);
               Pn532_uc_TxIndex++;

               if (Pn532_uc_TxIndex >= PN532_TX_IN_LIST_PASSIVE_TARGET_FRAME_LENGTH)
               {
                  Pn532_uc_RxIndex = 0U;
                  Pn532_us_TimeoutCounter = 0U;
               }
               else
               {
                  /* RF config frame has not been fully sent yet. */
               }
            }
            else
            {
               /* Nothing to do. */
            }
         }
         else
         {
            if (UART_RX_READY == Uart_gt_GetReceiveStatus(PN532_UART_RX))
            {
               Uart_gv_Receive(PN532_UART_RX, &Pn532_auc_ReceptionBuffer[Pn532_uc_RxIndex]);

               Pn532_us_TimeoutCounter = 0U;
               Pn532_uc_RxIndex++;

               if (Pn532_uc_RxIndex >= PN532_ACK_FRAME_LENGTH)
               {
                  Pn532_uc_RxIndex = 0U;
                  Pn532_t_State = PN532_STATE_READ_TARGET;

                  for (uc_I = 0U; uc_I < PN532_ACK_FRAME_LENGTH; uc_I++)
                  {
                     if (Pn532_auc_ReceptionBuffer[uc_I] != Pn532_kauc_RxAckPositiveFrame[uc_I])
                     {
                        Pn532_gv_Init();
                        uc_I = PN532_ACK_FRAME_LENGTH;
                     }
                     else
                     {
                     }
                  }
               }
               else
               {
               }
            }
            else
            {
               Pn532_us_TimeoutCounter++;
               if (Pn532_us_TimeoutCounter >= PN532_TIMEOUT_COUNT)
               {
                  Pn532_gv_Init();
               }
            }
         }

         break;
      }

      /*-------------------------------------------------------------------------------------------------------------*/
      case PN532_STATE_READ_TARGET:
      {
         if (UART_RX_READY == Uart_gt_GetReceiveStatus(PN532_UART_RX))
         {
            Uart_gv_Receive(PN532_UART_RX, &Pn532_auc_ReceptionBuffer[Pn532_uc_RxIndex]);

            Pn532_us_TimeoutCounter = 0U;
            Pn532_uc_RxIndex++;

            if (Pn532_uc_RxIndex >= PN532_RX_IN_LIST_PASSIVE_TARGET_FRAME_LENGTH)
            {
               for (uc_I = 0U; uc_I < PN532_RX_IN_LIST_PASSIVE_TARGET_ID_OFFSET; uc_I++)
               {
                  if (Pn532_auc_ReceptionBuffer[uc_I] != Pn532_kauc_RxInListPassiveTargetFrame[uc_I])
                  {
                     Pn532_gv_Init();
                     uc_I = PN532_ACK_FRAME_LENGTH;
                  }
                  else
                  {
                  }
               }

               if (PN532_STATE_READ_TARGET == Pn532_t_State)
               {
                  Pn532_uc_TxIndex = 0U;

                  ul_TargetID = ((Pn532_auc_ReceptionBuffer[PN532_RX_IN_LIST_PASSIVE_TARGET_ID_OFFSET] << 24U)
                     | (Pn532_auc_ReceptionBuffer[PN532_RX_IN_LIST_PASSIVE_TARGET_ID_OFFSET + 1U] << 16U)
                     | (Pn532_auc_ReceptionBuffer[PN532_RX_IN_LIST_PASSIVE_TARGET_ID_OFFSET + 2U] << 8U)
                     | (Pn532_auc_ReceptionBuffer[PN532_RX_IN_LIST_PASSIVE_TARGET_ID_OFFSET + 3U]));

                  Pn532_gm_WriteTargetId(&ul_TargetID);

                  Pn532_t_State = PN532_STATE_IDLE;
               }
               else
               {
               }
            }
            else
            {
            }
         }
         else if (0U != Pn532_uc_RxIndex)
         {
            Pn532_us_TimeoutCounter++;
            if (Pn532_us_TimeoutCounter >= PN532_TIMEOUT_COUNT)
            {
               Pn532_gv_Init();
            }
         }
         else
         {
         }
      } /* @suppress("No break at end of case") */

      /*-------------------------------------------------------------------------------------------------------------*/
      case PN532_STATE_IDLE:
      {
         if (Pn532_us_ScanTargetCounter >= PN532_SCAN_TARGET_PERIODICITY_COUNT)
         {
            Pn532_us_ScanTargetCounter -= PN532_SCAN_TARGET_PERIODICITY_COUNT;
            Pn532_uc_TxIndex = 0U;
            Pn532_t_State = PN532_STATE_REQUEST_TARGET;
         }
         break;
      }

      default:
      {
         break;
      }
   }
   Pn532_us_ScanTargetCounter++;
}
