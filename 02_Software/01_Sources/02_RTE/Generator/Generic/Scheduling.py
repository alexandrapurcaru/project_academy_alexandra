import xlwings as xw
import os.path
from Common import *

class Runnable:
	def __init__(self, name):
		self.name = name
		self.runnables = []
	def add_Member(self, runnable):
		self.runnables.append(runnable)

class Scheduling:
	def __init__(self):
		self.runnable = []
		self.init = []
	def add_Runnable(self, runnable):
		self.runnable.append(runnable)
	def add_Init(self, init_run):
		self.init.append(init_run)
	def check_duplicate(self, name):
		len_runnable = len(self.runnable)
		for i in range(0, len_runnable):
			if name == self.runnable[i].name:
				return 1
		
		return 0

data = Scheduling()
log = Log()

def reader(application):
	wb_data = xw.Book(dirname + "/../Config/Scheduling.xlsm")
	len_data = int(wb_data.sheets["Scheduling"].range(TOTAL_LINES_POS).value)
	len_init = int(wb_data.sheets["Scheduling"].range(TOTAL_LINES_INIT_POS).value)
	matrix_data = wb_data.sheets["Scheduling"].range(SCHEDULING_TASK_N + str(START_CELL_POS), SCHEDULING_RUNNABLES + str(len_data + 1)).value
	array_data = wb_data.sheets["Scheduling"].range(SCHEDULING_INIT + str(START_CELL_POS), SCHEDULING_INIT + str(len_init + 1)).value
	wb_data.close()
	len_data = len_data - MINUS_VAL
	len_init = len_init - MINUS_VAL
	
	len_modules = len(application.module)
	
	for i in range(0, len_init):
		found = 0
		for j in range(0, len_modules):
			len_module_runnables = len(application.module[j].runnable)
			for k in range(0, len_module_runnables):
				if application.module[j].runnable[k].name == array_data[i]:
					application.module[j].runnable[k].used = 1
					data.add_Init(application.module[j].runnable[k])
					found = 1
		
		if found == 0:
			log.errors = log.errors + "Init function: " + array_data[i] + " not found!\n"
			return 1
	
	for i in range(0, len_data):
		if matrix_data[i][0] == None and matrix_data[i][1] == None:
			log.errors = log.errors + "Invalid syntax at Scheduling at the line " + str(i + MINUS_VAL + 1) + "!\n"
			return 1
		
		if matrix_data[i][0] != None:
			if check_ostask(matrix_data[i][0]) == 1:
				log.errors = log.errors + "Invalid task name in Scheduling at the line " + str(i + MINUS_VAL + 1) + ".(" + matrix_data[i][0] + ")\n"
			
			runnable = Runnable(matrix_data[i][0])
			if data.check_duplicate(runnable.name) == 1:
				log.errors = log.errors + "Scheduling:Task:" + runnable.name + " name is duplicate.\n"
			
			data.add_Runnable(runnable)

		if matrix_data[i][1] != None:
			found_run = 0
			for j in range(0, len_modules):
				len_module_runnables = len(application.module[j].runnable)
				for k in range(0, len_module_runnables):
					if application.module[j].runnable[k].name == matrix_data[i][1]:
						application.module[j].runnable[k].used = 1
						runnable.add_Member(application.module[j].runnable[k])
						found_run = 1
			
			if found_run == 0:
				log.errors = log.errors + "Runnable function: " + matrix_data[i][1] + " not found!\n"
				return 1
		
	return 0

def writer(application):
	Rte_Tasks = open(dirname+"/../../../09_Templates/Empty/Code/Generic/Empty_Source.c", 'r')
	lines_tasks = Rte_Tasks.readlines()
	Rte_Tasks.close()
	Rte_Tasks = open(dirname+"/../../Code/Config/Rte_Tasks.c", 'w')
	lines_tasks[C_FILE_N_POS] = " *    \\file       Rte_Tasks.c\n"
	lines_tasks[C_AUTHOR_POS] = " *    \\author     " + AUTHOR_N + "\n"
	lines_tasks[C_BRIEF_POS] = " *    \\brief      " + BRIEF + "\n"
	lines_tasks[C_BRIEF_POS + 1] = BRIEF_TASKS_C + "\n"
	lines_tasks[C_INCLUSIONS_POS] = "\n#include \"Rte.h\"\n" + "#include \"Os.h\"\n\n/* SWC Headers. */\n"
	lines_tasks[C_G_IMPL_FUNC_POS] = "\n/**\n * \\brief      Initializes RTE and all the SWCs.\n * \\param      -\n * \\return     -\n */\n" + "TASK(OS_INIT_TASK)\n{\n   Rte_Start();\n"
	
	len_modules = len(application.module)
	for i in range(0, len_modules):
		lines_tasks[C_INCLUSIONS_POS] = lines_tasks[C_INCLUSIONS_POS] + "#include \""+ application.module[i].name + ".h\"\n"
	
	len_data_init = len(data.init)
	for i in range(0, len_data_init):
		lines_tasks[C_G_IMPL_FUNC_POS] = lines_tasks[C_G_IMPL_FUNC_POS] + "   " + data.init[i].name +  "();\n"
	
	lines_tasks[C_INCLUSIONS_POS] = lines_tasks[C_INCLUSIONS_POS] + "\n"
	lines_tasks[C_G_IMPL_FUNC_POS] = lines_tasks[C_G_IMPL_FUNC_POS] + "}\n\n"
	
	len_data = len(data.runnable)
	for i in range(0, len_data):
		lines_tasks[C_G_IMPL_FUNC_POS] = lines_tasks[C_G_IMPL_FUNC_POS] + "/**\n * \\brief      Application specific " + data.runnable[i].name + " task.\n * \\param      -\n * \\return     -\n */\n" + "TASK(" + data.runnable[i].name + ")\n{\n"
		
		len_data_runnable = len(data.runnable[i].runnables)
		for j in range(0, len_data_runnable):
			lines_tasks[C_G_IMPL_FUNC_POS] = lines_tasks[C_G_IMPL_FUNC_POS] + "   " + data.runnable[i].runnables[j].name + "();\n"
		
		i = i + len_data_runnable - 1
		lines_tasks[C_G_IMPL_FUNC_POS] = lines_tasks[C_G_IMPL_FUNC_POS] + "}\n\n"
	
	Rte_Tasks.writelines(lines_tasks)
	Rte_Tasks.close()
	
	return 0