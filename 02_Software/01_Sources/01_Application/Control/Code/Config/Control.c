/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Control.c
 *    \author     Alexandra Purcaru
 *    \brief      Implements the Control functions.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include <Control.h>
#include "Rte_Control.h"
#include "Rte_UserM.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#define CONTROL_NR_OF_USERS             (3U)
#define CONTROL_PWM_STEP                (100U)

#define CONTROL_PWM_LEFT_MIN                 (2200U)
#define CONTROL_PWM_LEFT_MAX                 (9000U)
#define CONTROL_PWM_RIGHT_MIN                (6000U)
#define CONTROL_PWM_RIGHT_MAX                (11500U)
#define CONTROL_PWM_MIDDLE_MIN               (2200U)
#define CONTROL_PWM_MIDDLE_MAX               (4700U)

#define CONTROL_TIMEOUT_ENGINE_LEDS     (200U)
#define CONTROL_TIMEOUT_LEDS            (100U)
#define CONTROL_TIMEOUT_MEMORY          (100U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

typedef enum
{
   CONTROL_INIT,
   CONTROL_RUNTIME,
} Control_StateType;

Rte_ParameterType Control_at_Users[CONTROL_NR_OF_USERS] =
   {
      { 0xAAAAAAAA, 0xAA, 0xAA, 0xAA }, /* A */
      { 0xAAAAAAAA, 0xAA, 0xAA, 0xAA }, /* B */
      { 0xAAAAAAAA, 0xAA, 0xAA, 0xAA }, /* C */
   };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

Control_StateType Control_t_CurrentState = CONTROL_INIT;
uint8 Control_uc_CurrentUser = CONTROL_NR_OF_USERS;
uint8 Control_uc_EmptyUser = CONTROL_NR_OF_USERS;

Rte_ButtonCtrlType Control_t_LastButtonLeftLeft = RTE_BUTTON_NOT_PRESSED;
Rte_ButtonCtrlType Control_t_LastButtonLeftRight = RTE_BUTTON_NOT_PRESSED;
Rte_ButtonCtrlType Control_t_LastButtonRightLeft = RTE_BUTTON_NOT_PRESSED;
Rte_ButtonCtrlType Control_t_LastButtonRightRight = RTE_BUTTON_NOT_PRESSED;
Rte_ButtonCtrlType Control_t_LastButtonMiddleLeft = RTE_BUTTON_NOT_PRESSED;
Rte_ButtonCtrlType Control_t_LastButtonMiddleRight = RTE_BUTTON_NOT_PRESSED;

uint8 Control_LeftPressCounter = 0U;
uint8 Control_RightPressCounter = 0U;
uint8 Control_MiddlePressCounter = 0U;

uint8 Control_EngineCounter = 0U;
uint8 Control_MemoryCounter = 0U;
uint8 Control_LedCounter = 0U;

Rte_LedCtrlType Control_t_LedGreen;
Rte_LedCtrlType Control_t_LedYellow;
Rte_LedCtrlType Control_t_LedRed;
Rte_LedCtrlType Control_t_LedEngineGreen;
Rte_LedCtrlType Control_t_LedEngineYellow;
Rte_LedCtrlType Control_t_LedEngineRed;

boolean Control_b_EngineStart = FALSE;
boolean Control_b_EngineOn = FALSE;

boolean Control_b_NewUser = TRUE;
boolean Control_b_Delete = FALSE;
boolean Control_b_RamMirror = FALSE;

Rte_ParameterType Control_at_NewUser;

static uint8 Control_uc_PermissionWrite = 0U;
static uint8 Control_uc_PermissionRead = 0U;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

static void Control_v_CheckTag(void);
static void Control_v_WriteTag(void);

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

static void Control_v_CheckTag(void)
{
   uint8 uc_I;
   Rte_RfidType t_Tag;

   if (Rte_IsUpdated_MemoryOut_Read() == TRUE)
   {
      if (Control_b_RamMirror == FALSE)
      {
         if (Control_uc_PermissionRead < CONTROL_NR_OF_USERS)
         {
            Rte_Read_MemoryOut_Read(&Control_at_Users[Control_uc_PermissionRead]);

            Control_uc_PermissionRead++;
         }
         else
         {
            Control_b_RamMirror = TRUE;
         }
      }
      else
      {
         /* No data has been read from flash memory. */
      }
   }
   else
   {
      /* The ram mirror has been done. */
   }

   if (Rte_IsUpdated_RfidIn_UserTag() == TRUE)
   {
      Rte_Read_RfidIn_UserTag(&t_Tag);

      for (uc_I = 0U; uc_I < (CONTROL_NR_OF_USERS); uc_I++)
      {
         if (t_Tag == Control_at_Users[uc_I].t_Tag)
         {
            Control_uc_CurrentUser = uc_I;
            Control_b_NewUser = FALSE;

            Control_t_LedYellow = RTE_LED_ON;
            Rte_Write_LedOut_LedYellow(&Control_t_LedYellow);
         }
         else
         {
            /* The tag code read by the RFID module is not the same with the tag code from the flash memory. */
            if (Control_at_Users[uc_I].t_Tag == 0xFFFFFFFF)
            {
               Control_uc_EmptyUser = uc_I;
            }
            else
            {

            }
         }

      }

      /* The current tag is new. */
      if (Control_b_NewUser == TRUE)
      {
         Control_uc_CurrentUser = Control_uc_EmptyUser;
         Control_at_Users[Control_uc_CurrentUser].t_Tag = t_Tag;
      }
      else
      {
      }
   }
   else
   {
      /* There is no tag detected. */
   }
}

static void Control_v_WriteTag(void)
{
   if (Control_uc_PermissionWrite < CONTROL_NR_OF_USERS)
   {
      Rte_Write_MemoryIn_Write(&Control_at_Users[Control_uc_PermissionWrite]);
      Control_uc_PermissionWrite++;
   }
   else
   {
      Control_b_EngineStart = FALSE;
   }

}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Implements the application routine for the actuators according to the sensors.
 * \param      -
 * \return     -
 */
void Control_gv_Main(void)
{
   /* Tells if a code is already registered or not. */

   Rte_ButtonCtrlType t_ButtonLeftLeft;
   Rte_ButtonCtrlType t_ButtonLeftRight;
   Rte_ButtonCtrlType t_ButtonRightLeft;
   Rte_ButtonCtrlType t_ButtonRightRight;
   Rte_ButtonCtrlType t_ButtonMiddleLeft;
   Rte_ButtonCtrlType t_ButtonMiddleRight;
   Rte_ButtonCtrlType t_ButtonEngineOn;
   Rte_ButtonCtrlType t_ButtonEngineOff;

   switch (Control_t_CurrentState)
   {
      /* Reads the tag code. */
      case (CONTROL_INIT):
      {
         Control_v_CheckTag();
         if (Control_uc_CurrentUser < CONTROL_NR_OF_USERS)
         {
            Control_t_CurrentState = CONTROL_RUNTIME;
         }
         break;
      }

      /* Verifies if the tag code is already in memory or not and it is, the motors move in the right position. */
      case (CONTROL_RUNTIME):
      {
         Control_v_CheckTag();

         Rte_Read_ButtonIn_ButtonLeftSideMirrorLeft(&t_ButtonLeftLeft);
         Rte_Read_ButtonIn_ButtonLeftSideMirrorRight(&t_ButtonLeftRight);
         Rte_Read_ButtonIn_ButtonRightSideMirrorLeft(&t_ButtonRightLeft);
         Rte_Read_ButtonIn_ButtonRightSideMirrorRight(&t_ButtonRightRight);
         Rte_Read_ButtonIn_ButtonSteeringWheelUp(&t_ButtonMiddleLeft);
         Rte_Read_ButtonIn_ButtonSteeringWheelDown(&t_ButtonMiddleRight);
         Rte_Read_ButtonIn_ButtonEngineOn(&t_ButtonEngineOn);
         Rte_Read_ButtonIn_ButtonEngineOff(&t_ButtonEngineOff);

         if ((t_ButtonEngineOn == RTE_BUTTON_PRESSED) && (t_ButtonEngineOff == RTE_BUTTON_PRESSED))
         {
            Control_b_EngineStart = TRUE;
         }

         if ((t_ButtonLeftLeft == RTE_BUTTON_PRESSED) && (t_ButtonLeftRight == RTE_BUTTON_PRESSED))
         {
            Control_b_Delete = TRUE;
         }

         /* Middle Motor. */
         if (Control_t_LastButtonMiddleLeft == RTE_BUTTON_PRESSED)
         {
            if ((t_ButtonMiddleLeft == RTE_BUTTON_PRESSED) && (Control_MiddlePressCounter < 12))
            {
               Control_MiddlePressCounter++;
            }

            if (Control_MiddlePressCounter >= 12)
            {
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle - CONTROL_PWM_STEP) >= CONTROL_PWM_MIDDLE_MIN)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle -= CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle = CONTROL_PWM_MIDDLE_MIN;
               }
            }

            if (t_ButtonMiddleLeft == RTE_BUTTON_NOT_PRESSED)
            {
               Control_MiddlePressCounter = 0U;
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle - CONTROL_PWM_STEP) >= CONTROL_PWM_MIDDLE_MIN)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle -= CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle = CONTROL_PWM_MIDDLE_MIN;
               }
            }
         }
         else if (Control_t_LastButtonMiddleRight == RTE_BUTTON_PRESSED)
         {
            if ((t_ButtonMiddleRight == RTE_BUTTON_PRESSED) && (Control_MiddlePressCounter < 12))
            {
               Control_MiddlePressCounter++;
            }

            if (Control_MiddlePressCounter >= 12)
            {
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle + CONTROL_PWM_STEP) <= CONTROL_PWM_MIDDLE_MAX)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle += CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle = CONTROL_PWM_MIDDLE_MAX;
               }
            }

            if (t_ButtonMiddleRight == RTE_BUTTON_NOT_PRESSED)
            {
               Control_MiddlePressCounter = 0U;
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle + CONTROL_PWM_STEP) <= CONTROL_PWM_MIDDLE_MAX)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle += CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle = CONTROL_PWM_MIDDLE_MAX;
               }
            }
         }

         /* Left Motor. */
         if (Control_t_LastButtonLeftLeft == RTE_BUTTON_PRESSED)
         {
            if ((t_ButtonLeftLeft == RTE_BUTTON_PRESSED) && (Control_LeftPressCounter < 12))
            {
               Control_LeftPressCounter++;
            }

            if (Control_LeftPressCounter >= 12)
            {
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyLeft - CONTROL_PWM_STEP) >= CONTROL_PWM_LEFT_MIN)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyLeft -= CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyLeft = CONTROL_PWM_LEFT_MIN;
               }
            }

            if (t_ButtonLeftLeft == RTE_BUTTON_NOT_PRESSED)
            {
               Control_LeftPressCounter = 0U;
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyLeft - CONTROL_PWM_STEP) >= CONTROL_PWM_LEFT_MIN)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyLeft -= CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyLeft = CONTROL_PWM_LEFT_MIN;
               }
            }
         }
         else if (Control_t_LastButtonLeftRight == RTE_BUTTON_PRESSED)
         {
            if ((t_ButtonLeftRight == RTE_BUTTON_PRESSED) && (Control_LeftPressCounter < 12))
            {
               Control_LeftPressCounter++;
            }

            if (Control_LeftPressCounter >= 12)
            {
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyLeft + CONTROL_PWM_STEP) <= CONTROL_PWM_LEFT_MAX)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyLeft += CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyLeft = CONTROL_PWM_LEFT_MAX;
               }
            }

            if (t_ButtonLeftRight == RTE_BUTTON_NOT_PRESSED)
            {
               Control_LeftPressCounter = 0U;
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyLeft + CONTROL_PWM_STEP) <= CONTROL_PWM_LEFT_MAX)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyLeft += CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyLeft = CONTROL_PWM_LEFT_MAX;
               }
            }
         }

         /* Right motor. */
         if (Control_t_LastButtonRightLeft == RTE_BUTTON_PRESSED)
         {
            if ((t_ButtonRightLeft == RTE_BUTTON_PRESSED) && (Control_RightPressCounter < 12))
            {
               Control_RightPressCounter++;
            }

            if (Control_RightPressCounter >= 12)
            {
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyRight - CONTROL_PWM_STEP) >= CONTROL_PWM_RIGHT_MIN)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyRight -= CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyRight = CONTROL_PWM_RIGHT_MIN;
               }
            }

            if (t_ButtonRightLeft == RTE_BUTTON_NOT_PRESSED)
            {
               Control_RightPressCounter = 0U;
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyRight - CONTROL_PWM_STEP) >= CONTROL_PWM_RIGHT_MIN)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyRight -= CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyRight = CONTROL_PWM_RIGHT_MIN;
               }
            }
         }
         else if (Control_t_LastButtonRightRight == RTE_BUTTON_PRESSED)
         {
            if ((t_ButtonRightRight == RTE_BUTTON_PRESSED) && (Control_RightPressCounter < 12))
            {
               Control_RightPressCounter++;
            }

            if (Control_RightPressCounter >= 12)
            {
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyRight + CONTROL_PWM_STEP) <= CONTROL_PWM_RIGHT_MAX)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyRight += CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyRight = CONTROL_PWM_RIGHT_MAX;
               }
            }

            if (t_ButtonRightRight == RTE_BUTTON_NOT_PRESSED)
            {
               Control_RightPressCounter = 0U;
               if ((Control_at_Users[Control_uc_CurrentUser].t_DutyRight + CONTROL_PWM_STEP) <= CONTROL_PWM_RIGHT_MAX)
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyRight += CONTROL_PWM_STEP;
               }
               else
               {
                  Control_at_Users[Control_uc_CurrentUser].t_DutyRight = CONTROL_PWM_RIGHT_MAX;
               }
            }
         }

         Rte_Write_ServoOut_ServoLeftSideMirror(&Control_at_Users[Control_uc_CurrentUser].t_DutyLeft);
         Rte_Write_ServoOut_ServoRightSideMirror(&Control_at_Users[Control_uc_CurrentUser].t_DutyRight);
         Rte_Write_ServoOut_ServoSteeringWheel(&Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle);

         Control_t_LastButtonLeftLeft = t_ButtonLeftLeft;
         Control_t_LastButtonLeftRight = t_ButtonLeftRight;
         Control_t_LastButtonRightLeft = t_ButtonRightLeft;
         Control_t_LastButtonRightRight = t_ButtonRightRight;
         Control_t_LastButtonMiddleLeft = t_ButtonMiddleLeft;
         Control_t_LastButtonMiddleRight = t_ButtonMiddleRight;

         if (Control_b_EngineStart == TRUE)
         {
            Control_b_NewUser = FALSE;

            Control_v_WriteTag();

            Control_t_LedEngineYellow = RTE_LED_ON;
            Control_t_LedEngineRed = RTE_LED_ON;

            Rte_Write_LedOut_LedEngineYellow(&Control_t_LedEngineYellow);
            Rte_Write_LedOut_LedEngineRed(&Control_t_LedEngineRed);

            if (Control_EngineCounter < CONTROL_TIMEOUT_ENGINE_LEDS)
            {
               Control_EngineCounter++;
            }
            else
            {
               Control_t_LedEngineYellow = RTE_LED_OFF;
               Control_t_LedEngineRed = RTE_LED_OFF;
               Control_t_LedEngineGreen = RTE_LED_ON;

               Rte_Write_LedOut_LedEngineYellow(&Control_t_LedEngineYellow);
               Rte_Write_LedOut_LedEngineRed(&Control_t_LedEngineRed);
               Rte_Write_LedOut_LedEngineGreen(&Control_t_LedEngineGreen);

               Control_b_EngineOn = TRUE;
               Control_b_EngineStart = FALSE;
               Control_EngineCounter = 0U;
            }
         }
         else
         {

         }

         if (Control_b_Delete == TRUE)
         {
            Control_at_Users[Control_uc_CurrentUser].t_Tag = 0xFFFFFFFF;
            Control_at_Users[Control_uc_CurrentUser].t_DutyLeft = 0xFFFF;
            Control_at_Users[Control_uc_CurrentUser].t_DutyRight = 0xFFFF;
            Control_at_Users[Control_uc_CurrentUser].t_DutyMiddle = 0xFFFF;

            Control_v_WriteTag();

            Control_t_LedRed = RTE_LED_ON;

            Rte_Write_LedOut_LedRed(&Control_t_LedRed);

            Control_b_Delete = FALSE;
         }
         else
         {

         }

         if ((Control_b_EngineOn == TRUE))
         {
            Control_t_LedRed = RTE_LED_ON;

            Rte_Write_LedOut_LedRed(&Control_t_LedRed);

            if (Control_EngineCounter < CONTROL_TIMEOUT_ENGINE_LEDS)
            {
               Control_EngineCounter++;
            }
            else
            {
               Control_t_LedRed = RTE_LED_OFF;

               Rte_Write_LedOut_LedRed(&Control_t_LedRed);

               Control_b_EngineOn = FALSE;
            }
         }
         else
         {

         }

         break;
      }
   }
}
