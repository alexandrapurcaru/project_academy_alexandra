/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Control.h
 *    \author     RTE Generator
 *    \brief      Generated file - shall not be manually edited. 
 *                Implements all the RTE read and write operations that are performed by the Control software
 *                component.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef RTE_CONTROL_H
#define RTE_CONTROL_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Rte.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#define Rte_Read_ButtonIn_ButtonLeftSideMirrorLeft(x) ((*x) = Rte_ButtonOut_ButtonLeftSideMirrorLeft_Buf)
#define Rte_Read_ButtonIn_ButtonLeftSideMirrorRight(x) ((*x) = Rte_ButtonOut_ButtonLeftSideMirrorRight_Buf)
#define Rte_Read_ButtonIn_ButtonRightSideMirrorLeft(x) ((*x) = Rte_ButtonOut_ButtonRightSideMirrorLeft_Buf)
#define Rte_Read_ButtonIn_ButtonRightSideMirrorRight(x) ((*x) = Rte_ButtonOut_ButtonRightSideMirrorRight_Buf)
#define Rte_Read_ButtonIn_ButtonSteeringWheelUp(x) ((*x) = Rte_ButtonOut_ButtonSteeringWheelUp_Buf)
#define Rte_Read_ButtonIn_ButtonSteeringWheelDown(x) ((*x) = Rte_ButtonOut_ButtonSteeringWheelDown_Buf)
#define Rte_Read_ButtonIn_ButtonEngineOn(x) ((*x) = Rte_ButtonOut_ButtonEngineOn_Buf)
#define Rte_Read_ButtonIn_ButtonEngineOff(x) ((*x) = Rte_ButtonOut_ButtonEngineOff_Buf)

#define Rte_Write_LedOut_LedGreen(x) (Rte_LedIn_LedGreen_Buf = (*x))
#define Rte_Write_LedOut_LedYellow(x) (Rte_LedIn_LedYellow_Buf = (*x))
#define Rte_Write_LedOut_LedRed(x) (Rte_LedIn_LedRed_Buf = (*x))
#define Rte_Write_LedOut_LedEngineGreen(x) (Rte_LedIn_LedEngineGreen_Buf = (*x))
#define Rte_Write_LedOut_LedEngineYellow(x) (Rte_LedIn_LedEngineYellow_Buf = (*x))
#define Rte_Write_LedOut_LedEngineRed(x) (Rte_LedIn_LedEngineRed_Buf = (*x))

#define Rte_Write_ServoOut_ServoLeftSideMirror(x) (Rte_ServoIn_ServoLeftSideMirror_Buf = (*x))
#define Rte_Write_ServoOut_ServoRightSideMirror(x) (Rte_ServoIn_ServoRightSideMirror_Buf = (*x))
#define Rte_Write_ServoOut_ServoSteeringWheel(x) (Rte_ServoIn_ServoSteeringWheel_Buf = (*x))

#define Rte_IsUpdated_RfidIn_UserTag() (Rte_Rfid_RfidIn_UserTag_UpdateFlag)
#define Rte_Read_RfidIn_UserTag(x) \
   do \
   { \
      (*x) = Rte_Rfid_UserTag_Buf; \
      Rte_Rfid_RfidIn_UserTag_UpdateFlag = FALSE; \
   } while(0)

#define Rte_IsUpdated_MemoryOut_Read() (Rte_MemoryRead_MemoryOut_Read_UpdateFlag)
#define Rte_Read_MemoryOut_Read(x) \
   do \
   { \
      (*x) = Rte_MemoryRead_Read_Buf; \
      Rte_MemoryRead_MemoryOut_Read_UpdateFlag = FALSE; \
   } while(0)

#define Rte_Write_MemoryIn_Write(x) \
   do \
   { \
      Rte_MemoryWrite_Write_Buf = (*x); \
      Rte_MemoryWrite_MemoryOut_Write_UpdateFlag = TRUE; \
   } while(0)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* RTE_CONTROL_H */